jQuery(document).ready(function() {
	jQuery("table tbody").on('click', 'tr',(function() {
		if(typeof $(this).attr('data-href') !== "undefined")
			window.location.href = jQuery(this).attr('data-href');
	}));

	jQuery("#user_id_self").click(function() {
		jQuery('#Goods_user_id').attr('value', jQuery(this).attr('data'));
	});

	jQuery("#adminAccess").click(function() {
		window.location.href = '/backend/users/adminAccess/id/' + jQuery(this).attr('data-id');
	});

	jQuery("#buyGoods").click(function() {
		window.location.href = '/goods/index/id/' + jQuery(this).attr('goods-id') + '/buy';
	});

	jQuery("#cancel").click(function() {
		window.location.href = '/order/cancel/id/' + jQuery(this).attr('goods-id');
	});

	jQuery("#fullBuy").click(function() {
		window.location.href = '/order/fullBuy/id/' + jQuery(this).attr('goods-id');
	});

	jQuery("#adm-site > a").click(function() {
		window.open(this.href);
		return false;
	});
});

<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'chv_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);

<?php
/**
 * Функции для работы в представлениях
 */
class WorkView {

    # Модификаторы вывода: $modif
    const STRING_PRINT = 0; // Вывод в формате string
    const INTEGER_PRINT = 1; // Вывод в формате integer

    /**
     * Количество новых сообщений
     */
    public static function countNewMail($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $count = count(Mail::model()->findAll('from_id=:my_id && status=:status_new', array(':my_id'=>$myId, ':status_new'=>4)));
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else if ($count >= 1) {
            $getCount = "<span class='label label-danger'>".$count."</span>";
            return $getCount;
        }
        return false;
    }

    /**
     * Количество заказанных товаров у пользователя
     */
    public static function countNewGoods($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $count = count(Goods::model()->findAll('user_id=:my_id && status=:status', array(':my_id'=>$myId,':status'=>1)));
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else if ($count >= 1){
            $getCount = "<span class='label label-danger'>".$count."</span>";
            return $getCount;
        }
        return false;
    }

    /**
     * Количество товаров которые заказали другие пользователи
     */
    public static function countNewOrder($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $count = count(Order::model()->findAll('id_user=:my_id && status=:status', array(':my_id'=>$myId,':status'=>1)));
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else if ($count >= 1){
            $getCount = "<span class='label label-danger'>".$count."</span>";
            return $getCount;
        }
        return false;
    }

    /**
     * Количество новых отзывов о пользователе
     */
    public static function countNewRevie($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $count = count(Revie::model()->findAll('from_id=:id && status=:sts', array(':id'=>$myId, ':sts'=>1)));
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else if ($countRevie >= 1) {
            $getCount = "<span class='label label-danger'>".$count."</span>";
            return $getCount;
        }
        return false;
    }

    /**
     * Количество в суме: заказанных товаров у пользователя, заказанных товаров пользователем, новых отзывов
     */
    public static function countOrderGoodsRevie($modif = STRING_PRINT) {
        $count = self::countNewGoods(INTEGER_PRINT) + self::countNewOrder(INTEGER_PRINT) + self::countNewRevie(INTEGER_PRINT);
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else if ($count >= 1) {
            $getCount = 'Торговля <span class="label label-danger">'.$count.'</span>';
        } else {
            $getCount = 'Торговля';
        }
        return $getCount;
    }

    /**
     * Количество не промодерированных пользовательских серверов
     */
    public static function countCustomServer($modif = STRING_PRINT) {
        $count = count(TgameServer::model()->findAll('status=:status', array(':status'=>'0')));
        if ($modif == INTEGER_PRINT) {
            $getCount = $count;
            return $getCount;
        } else {
            $getCount = "<span class='label label-danger'>".$count."</span>";
            return $getCount;
        }
    }

    /**
     * Текст для вывода статического текста с БД
     * $path - путь, прописанный в БД;
     */
    public static function textPage($path) {
        $textPage = Text::model()->findAll('path=:path', array('path'=>$path));
        return $textPage;
    }

    /**
     * Вывод числового значения заданной цены
     * $pricesKey - ключ цены;
     */
    public static function pricesService($pricesKey) {
        $modelPrice = Prices::model()->findAll('keyPrice=:pricesKey', array(':pricesKey'=>$pricesKey));
        return $modelPrice[0]->price;
    }

    /**
     * Количество денег пользователя
     */
    public static function myCurren($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $modelUsers = Users::model()->findByPk($myId);
        $getCount = 0;
        if ($modif == INTEGER_PRINT) {
            $getCount = $modelUsers->curr;
            return $getCount;
        } else {
            $getCount = $modelUsers->curr . '$';
            return $getCount;
        }
    }

    /**
     * Количество денег в корзине у пользователя
     */
    public static function myCurrenBasket($modif = STRING_PRINT) {
        $myId = Yii::app()->user->id;
        $basketCurren = Order::model()->with('good')->findAll('t.id_user=:id_user && t.status=:status', array(':id_user'=>$myId, ':status'=>1));
        $count = 0;
        foreach ($basketCurren as $arr) {
            $count += $arr->good->count * $arr->good->cost;
        }
        if ($modif == INTEGER_PRINT) {
            return $count;
        } else {
            return $count . '$';
        }
    }

    /**
     * То же что и функция pricesService() xD
     * Лень смотреть де она используется и заменить, поэтому пускай будет с:
     */
    public static function pricesPurch($key) {
        $model = Prices::model()->findAll('keyPrice=:key', array(':key'=>$key));
        foreach ($model as $modelOne) {
            $res = $modelOne->price;
        }
        return $res;
    }

    /**
     * Вывод картинки категории, если есть
     * иначе вывод стандартной картинки
     */
    public static function imageGame($id) {
        $fileName = './images/game_icon/' . $id . '.jpg';
        if (file_exists($fileName)) {
            return $id;
        } else {
            return 0;
        }
    }

}

<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $vk_id
 * @property string $nick
 * @property string $pass
 * @property string $pol
 * @property string $rule
 * @property string $ban
 * @property string $ban_time
 * @property string $lvl_ban
 * @property string $email
 * @property integer $icq
 * @property string $skype
 * @property string $date_reg
 * @property string $date_last
 * @property string $name
 * @property string $surname
 * @property string $patron
 * @property string $online
 */
class Users extends CActiveRecord
{

	public $verifyCode;
	public $nick;
	public $pass;
	public $name;
	public $email;
	public $online;
	public $date_reg;
	public $date_last;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
/*		return array(
			array('nick, pass, email, name, pol', 'required'),
			array('ban, icq', 'numerical', 'integerOnly'=>true),
			array('vk_id, email, skype', 'length', 'max'=>50),
			array('date_reg, date_last', 'numerical', 'max'=>16, 'integerOnly'=>true),
			array('online', 'numerical', 'max'=>1, 'integerOnly'=>true),
			array('pass', 'length', 'max'=>250),
			array('nick, pass, name', 'length', 'min'=>6,'max'=>23),
			array('rule', 'length', 'max'=>1),
			array('surname, patron', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vk_id, nick, pass, pol, rule, ban, email, icq, skype, date_reg, date_last, name, surname, patron, online', 'safe', 'on'=>'search'),
		);*/
return array(
	array('nick, pass, email, name, pol', 'required'),
	array('icq', 'numerical', 'integerOnly'=>true),
	array('email', 'email', 'allowEmpty'=>false,'validateIDN'=>true),
	array('vk_id, email, skype', 'length', 'max'=>50, 'min'=>4),
	array('pass', 'length', 'max'=>250, 'min'=>6),
	array('nick, ban_time, date_reg, date_last, name', 'length', 'max'=>16),
	array('pol, rule, ban, online', 'length', 'max'=>1),
	array('lvl_ban', 'length', 'max'=>6),
	array('surname, patron', 'length', 'max'=>32),
	// The following rule is used by search().
	// @todo Please remove those attributes that should not be searched.
	array('id, vk_id, nick, pass, pol, rule, ban, ban_time, lvl_ban, email, icq, skype, date_reg, date_last, name, surname, patron, online', 'safe', 'on'=>'search'),
);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nick' => 'Ник / Логин',
			'pass' => 'Пароль',
			'repass' => 'Повторите пароль',
			'email' => 'E-mail',
			'name' => 'Имя',
			'verifyCode' => 'Введите код с картинки',
			'pol' => 'Пол',
			'surname' => 'Фамилия',
			'patron' => 'Отчество',
			'date_reg' => 'Дата регистрации',
			'date_last' => 'Последний вход',
			'skype' => 'Skype',
			'icq' => 'ICQ',
			'ban' => 'Бан',
		);
	}

	public function validatePassword($pass)
	{
		return CPasswordHelper::verifyPassword($pass,$this->pass);
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */

	protected function beforeSave()
    {
         if(parent::beforeSave())
         {
            if($this->isNewRecord)
            {
                $this->date_reg = time();
                $this->date_last = time();
                $this->online = 0;
                $this->pass = $this->hashPassword($this->pass);
            }
            return true;
         }
        return false;
    }

	public function hashPassword($pass)
	{
		return CPasswordHelper::hashPassword($pass);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vk_id',$this->vk_id,true);
		$criteria->compare('nick',$this->nick,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('pol',$this->pol,true);
		$criteria->compare('rule',$this->rule,true);
		$criteria->compare('ban',$this->ban,true);
		$criteria->compare('ban_time',$this->ban_time,true);
		$criteria->compare('lvl_ban',$this->lvl_ban,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('icq',$this->icq);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('date_reg',$this->date_reg,true);
		$criteria->compare('date_last',$this->date_last,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('patron',$this->patron,true);
		$criteria->compare('online',$this->online, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}

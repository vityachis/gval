<?php

/**
 * This is the model class for table "{{revie}}".
 *
 * The followings are the available columns in table '{{revie}}':
 * @property integer $id
 * @property integer $from_id
 * @property integer $user_id
 * @property string $ret
 * @property string $cr_date
 * @property string $text
 * @property string $status
 */
class Revie extends CActiveRecord
{

	private $_userNick;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{revie}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, ret, cr_date', 'required'),
			array('from_id, user_id', 'numerical', 'integerOnly'=>true),
			array('ret, status', 'length', 'max'=>1),
			array('cr_date', 'length', 'max'=>16),
			array('text', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_id, user_id, ret, cr_date, text, status, userNick', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_id' => 'Для',
			'user_id' => 'Оставил',
			'ret' => 'Рейтинг',
			'cr_date' => 'Дата создания',
			'text' => 'Текст',
			'status' => 'Статус',
			'userNick' => 'Оставил',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user');

		$criteria->condition = 't.from_id=:id';
		$criteria->params = array(':id'=>Yii::app()->user->id);

		$criteria->compare('id',$this->id);
		$criteria->compare('from_id',$this->from_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('ret',$this->ret,true);
		$criteria->compare('cr_date',$this->cr_date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('status',$this->status,true);

		$criteria->compare('user.nick',$this->userNick,true);

		$sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes['userNick'] = array('asc' => 'user.nick','desc' => 'user.nick desc');
        $sort->attributes['ret'] = array('asc' => 't.ret','desc' => 't.ret desc');
        $sort->attributes['text'] = array('asc' => 't.text','desc' => 't.text desc');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25),
			'sort'=>$sort,
		));
	}

	public function beforeSave() {
        if (parent::beforeSave()) {
        	if ($this->isNewRecord) {
        		$this->cr_date = time();
        		$this->from_id = $id;
        	}
        	return true;
        } else {
        	return false;
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Revie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	==============================================================
	============= Start: Поиск по связанным таблицам =============
	==============================================================
	*/

	/**
	 * Поле: user_id
	 */
	public function getUserNick() {
	    if ($this->_userNick === null && $this->userNick !== null) {
	        $this->_userNick = $this->user->nick;
	    }
	    return $this->_userNick;
	}
	public function setUserNick($value)
	{
	    $this->_userNick = $value;
	}

	/*
	============================================================
	============= End: Поиск по связанным таблицам =============
	============================================================
	*/
}

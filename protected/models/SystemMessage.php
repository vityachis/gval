<?
/**
 * Класс с текстом для генерации сообщений от системы
 */
class SystemMessage {

    public static function goodsOrders($href, $name = 'Неизвестно') {
        $myId = Yii::app()->user->id;
        $myName = Users::model()->findByPk($myId)->nick;
        $mess = 'Ваш товар (' . $name . ') был заказан пользователем: ' . $myName . '. Вы можете написать ' . CHtml::link('ему сообщение', array('mail/index', 'id'=>$myId)) . ' для обсуждения условий передачи товара!';
        return $mess;
    }

    public static function goodsPurch($nick_id, $name = 'Неизвестно') {
        $mess = 'Ваш товар (' . $name . ') успешно куплен и передан пользователю ' . Users::model()->findByPk($nick_id)->nick . '! Деньги были начислены на Ваш счет! Поздравляем с успешной сделкой!';
        return $mess;
    }

    public static function goodsCancel($name = 'Неизвестно') {
        $myId = Yii::app()->user->id;
        $myName = Users::model()->findByPk($myId)->nick;
        $mess = 'Заказ (' . $name . ') был отменем пользователем: ' . $myName . '. И поставлен опять в список активных товаров!';
        return $mess;
    }

    public static function customServer($act, $name, $href, $userNick = 'System', $userId = 777) {
        if ($act == 'ok') {
            $mess = 'Созданный вами сервер (<a href="'.$href.'">'.$name.'</a>), успешно прошел модерацию и был размешен в нашем ТОП\'е! Разместил: <a></a>';
        } else if ($act == 'no') {
            $mess = 'Созданный вами сервер (<a href="'.$href.'">'.$name.'</a>), <strong>НЕ</strong> прошел модерацию и <strong>НЕ</strong> будет размешен в нашем ТОП\'е! Извините пожалуйста!';
        }
        return $mess;
    }
}

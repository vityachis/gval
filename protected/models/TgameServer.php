<?php

/**
 * This is the model class for table "{{tgame_server}}".
 *
 * The followings are the available columns in table '{{tgame_server}}':
 * @property integer $id
 * @property string $name
 * @property integer $ret
 * @property integer $game_id
 * @property string $status
 * @property string $href
 * @property integer $user_id
 * @property integer $cr_date
 * @property integer $moder_id
 */
class TgameServer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tgame_server}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, game_id, href', 'required'),
			array('ret, game_id, user_id, cr_date, moder_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('href', 'length', 'max'=>250),
			array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, ret, game_id, status, href, user_id, cr_date, moder_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'ret' => 'Рейтинг',
			'game_id' => 'Игра',
			'status' => 'Статус',
			'href' => 'Ссылка',
			'user_id' => 'Создатель',
			'cr_date' => 'Дата создания',
			'moder_id' => 'Модератор',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user', 'game');

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('ret',$this->ret);
		$criteria->compare('game_id',$this->game_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('href',$this->href,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('cr_date',$this->cr_date);
		$criteria->compare('moder_id',$this->moder_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() {
        if (parent::beforeSave())
        {
        	if ($this->isNewRecord) {
	        	$this->cr_date = time();
	        	$this->status = 0;
	        	$this->ret = 0;
	        	$this->user_id = Yii::app()->user->id;
        	} else {
        		$this->moder_id;
        	}
        	return true;
        } else {
        	return false;
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TgameServer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?
/**
 * Класс с текстом для Alert'ов
 */
class AlertMess {
    const DANGER_NO_AUTH        = 'Для работы с сайтом, нужно пройти авторизацию!';
    const NO_BUY_THEIR_GOODS    = 'Вы не можете купить свой товар!';
    const GOODS_ALREADY_ORDERED = 'Данный товар уже был заказан!';
    const FULLY_PURCH           = 'Товар полностью куплен!';
    const ERROR_BUY             = 'Ошибка покупки!';
    const SEND_ERROR_SYSTEM     = 'Товар заказан! Но сообщение не отправлено продавцу!';
    const GOODS_ORDERED         = 'Товар заказан!';
    const NOT_ENOUGH_MONEY      = 'У вас не достаточно денег для покупки!';
    const SUCCESS_CREATED       = 'Товар успешно создан!';
    const CHANGES_SAVED         = 'Изменения сохранены!';
    const EDIT_SOMEONE_PRODUCT  = 'Вы не можете отредактировать чужой товар!';
    const GOODS_ORDERED_NO_EDIT = 'Данный товар был заказан, редактировать его невозможно!';
    const FULLY_PURCH_NO_EDIT   = 'Данный товар полностью куплен, редактировать его не возможно!';
    const MESS_SEND             = 'Сообщение отправлено!';
    const MESS_NO_SEND_MY       = 'Вы не можете отправить сообщение самому себе!';
    const MESS_NO_SEND_SYSTEM   = 'Вы не можете отправить сообщение системе!';
    const NO_FULL_BUY_ORDER     = 'Вы не можете полностью оформить покупку уже полностью купленного товара!';
    const NO_FULL_BUY_SOMEONE   = 'Вы не можете оформить полную покупку чужого заказа!';
    const MARKED_FULL_BUY       = 'Товар отмечен как переданный! Спасибо за покупку!';
    const CANCEL_NO_FULL_BUY    = 'Вы не можете отменить уже полностью купленный товар!';
    const CANCEL_OK             = 'Сделка успешно отменена!';
    const TIME_CANCEL_ERROR     = 'Во время отмены заказа, возникла ошибка!';
    const ERROR_CANCEL_SELLER   = 'Вы не можете отменить заказ! У продавца недостаточно денег для возврата!';
    const CANCEL_SOMEONE_ORDER  = 'Вы не можете отменить чужой заказ!';
    const FEEDBACK_SUCCESS      = 'Спасибо за обращение к нам. Мы постараемся ответить вам как можно скорее.';
    const ALREADY_AUTH          = 'Вы уже вошли в систему!';
    const ALREADY_REG           = 'Вы уже зарегистрированы!';
    const SUCCESS_REG           = 'Вы успешно зарегистрированы!';
    const SUCCESS_REVIE         = 'Отзыв успешно оставлен!';
    const NO_TWO_REVIE          = 'Вы не можете оставить еще один отзыв!';
    const REVIE_NO_MY           = 'Вы не можете оставить отзыв самому себе!';
    const REVIE_NO_SYSTEM       = 'Вы не можете оставить отзыв системе!';
    const GUEST_NO_UP_RATING    = 'Гость не может поднять рейтинг! Пожалуйста пройдите авторизацию!';
    const NO_RATING_SERV_SITE   = 'Рейтинг серверов сайта поднимается за счет успешных сделок!';
    const SERVER_NO_GAME        = 'Выбранный сервер не принадлежит выбранной игре!';
    const PART_NO_GAME          = 'Выбранная сторона не принадлежит выбранной игре!';
    const CUSTOM_SERVER_MODER   = 'Сервер отправлен на модерацию! И скоро появиться на странице серверов!';
    const NO_SERVER_CUSTOM_CURR = 'Денег для создания сервера не достаточно!';
    const ERROR_SERVICE_CURR    = 'Ошибка при создании нового пользовательского игрового сервера';
    const ERROR_ADDR_GET        = 'Ошибка запроса!';
    const MODER_CUSTOM_OK       = 'Сервер опубликован!';
    const MODER_CUSTOM_NO       = 'Сервер не прошел модерацию!';
    const RATING_UP             = 'Рейтинг сервера успешно поднят на 1!';
    const NO_RATING_UP          = 'На счету недостаточно денег для поднятия рейтинга!';
    const OK_SALLER             = 'Прочитайте правила, перед размещением товара!';
    const ALREADY_SALLER_OK     = 'Правила были прочитаны и приняты ранее!';
    const RULES_SALLER_OK       = 'Теперь вы можете создать товар для продажи!';
    const RULES_SALLER_NO       = 'Вы не согласились с правилами сайта!';
}

<?php
/* @var $this OrderController */

$this->pageTitle = Yii::app()->name . ' - Ваш список заказов';
?>

<h1 class="page-header">Ваши заказы</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (count($model) >= 1):?>
<table class="table table-condensed table-hover chip-offers">
    <thead>
    <tr>
        <th>игра</th>
        <th>сервер</th>
        <th class="text-center">сторона</th>
        <th class="text-center">продавец</th>
        <th class="text-right">название</th>
        <th class="text-right">тип товара</th>
        <th class="text-right">цена</th>
        <th class="text-right">количество</th>
        <th class="text-center">действия</th>
    </tr>
    </thead>
    <tbody>
        <?foreach($model as $modelAlert):?>
            <?if($modelUsers[$modelAlert->good->user_id]['online'] == 1)
                $statusOn = '<span class="label label-success">online</span>';
            if($modelUsers[$modelAlert->good->user_id]['online'] == 0) {
                $statusOn = '<span class="label label-danger">offline</span>';
            }
            ?>
            <tr>
                <td><?=$modelGame[$modelAlert->good->game_id]?></td>
                <td><?=$modelServ[$modelAlert->good->serv_id]?></td>
                <td class="text-center"><?=$modelPart[$modelAlert->good->part_id]?></td>
                <td class="text-center">
                    <div><?=$modelUsers[$modelAlert->good->user_id]['nick']?> <?=$statusOn?> </div>
                </td>
                <td class="text-right">
                    <div><?=$modelAlert->good->name?></div>
                </td>
                <td class="text-right">
                    <div><?=$modelTypeGoods[$modelAlert->good->type_good]?></div>
                </td>
                <td class="text-right">
                    <div><?=$modelAlert->good->count*$modelAlert->good->cost?>$</div>
                </td>
                <td class="text-right">
                    <div><?=$modelAlert->good->count?> единиц</div>
                </td>
                <?php $this->widget('bootstrap.widgets.TbModal', array(
                    'id' => 'cancel'.$modelAlert->id,
                    'header' => 'Отмена заказа: '.$modelAlert->good->name,
                    'content' => 'Если вы отмените заказ, деньги вернутся на Ваш счет, а заказ будет помещен в таблицу активных товаров',
                    'footer' => array(
                        TbHtml::submitButton('Да, я хочу отменить свой заказ', array('goods-id'=>$modelAlert->id, 'id'=>'cancel', 'data-dismiss' => 'modal', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                        TbHtml::button('Ой, не туда нажал', array('data-dismiss' => 'modal')),
                     ),
                )); ?>
                <?php $this->widget('bootstrap.widgets.TbModal', array(
                    'id' => 'fullBuy'.$modelAlert->id,
                    'header' => 'Товар: '.$modelAlert->good->name . ' получен?',
                    'content' => 'Если товар полностью получен, он будет удален с таблицы ваших заказов и с таблицы товаров сайта!',
                    'footer' => array(
                        TbHtml::submitButton('Да, товар был полностью получен', array('goods-id'=>$modelAlert->id, 'id'=>'fullBuy', 'data-dismiss' => 'modal', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)),
                        TbHtml::button('Отмена, товар еще не получен', array('data-dismiss' => 'modal')),
                     ),
                )); ?>
                <td class="text-right">
                    <?php echo TbHtml::button('Отменить заказ', array(
                        'data-toggle' => 'modal',
                        'data-target' => '#cancel'.$modelAlert->id,
                    )); ?>
                    <?php echo TbHtml::button('Товар получен', array(
                        'data-toggle' => 'modal',
                        'data-target' => '#fullBuy'.$modelAlert->id,
                        'color'=>TbHtml::BUTTON_COLOR_INFO,
                    )); ?>
                </td>
            </tr>
        <?endforeach?>
    </tbody>
</table>
<?$this->widget('CLinkPager', array(
    'header' => '',
    'pages' => $pages,
    'htmlOptions'=> array('class'=>'pagination'),
));?>
<?else:?>
<div class="alert alert-info">
    <strong>Извините, но в данной категории нет заказов =(</strong>
</div>
<?endif?>

<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
$this->pageTitle=Yii::app()->name . " - Создать игровой сервер для ТОПа";
?>

<h1 class="page-header">Создать игровой сервер для ТОПа</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelGameList'=>$modelGameList)); ?>

<span class="help-block">Создание нового пользовательского игрового сервера стоит: <?=WorkView::pricesService('customServer')?>$</span>

<?php
$this->pageTitle=Yii::app()->name . " - Модерация игровых серверов";
?>

<h1 class="page-header">Модерация игровых серверов</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (count($model) >= 1):?>
<table class="table table-condensed table-hover table-clickable chip-offers">
    <thead>
    <tr>
        <th>название</th>
        <th>сервер игры</th>
        <th>адрес</th>
        <th>создатель</th>
        <th>действия</th>
    </tr>
    </thead>
    <tbody>
        <?foreach($model as $modelAlert):?>
            <tr>
                <td><?=$modelAlert->name?></td>
                <td><?=$modelAlert->game->name?></td>
                <td><a href="<?=$modelAlert->href?>" target="_blank"><?=$modelAlert->href?></a></td>
                <td><a href="<?=$this->createUrl('users/index', array('id'=>$modelAlert->user->id))?>"><?=$modelAlert->user->nick?></a></td>
                <td width="20%">
                    <a href="<?=$this->createUrl('/tgameServer/index', array('id'=>$modelAlert->id, 'act'=>'ok'))?>"><button type="button" class="btn btn-info">Подтвердить</button></a>
                    <a href="<?=$this->createUrl('/tgameServer/index', array('id'=>$modelAlert->id, 'act'=>'no'))?>"><button type="button" class="btn btn-danger">Отменить</button></a>
                </td>
            </tr>
        <?endforeach?>
    </tbody>
</table>
<?$this->widget('CLinkPager', array(
    'header' => '',
    'pages' => $pages,
    'htmlOptions'=> array('class'=>'pagination'),
));?>
<?else:?>
<div class="alert alert-info">
    <strong>Извините, но серверов для модерации пока что нет!</strong>
</div>
<?endif?>

<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'tgame-server-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('class'=>'form-narrow'),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> заполнять обязательно.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'href'); ?>
		<?php echo $form->textField($model,'href',array('size'=>60,'maxlength'=>250)); ?>
		<span class="help-block">Ссылка на игровой сервер в виде: http://<?=$_SERVER['SERVER_NAME']?></span>
		<?php echo $form->error($model,'href'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'game_id'); ?>
		<?php echo $form->dropDownList($model,'game_id', TbHtml::listData($modelGameList, 'id', 'name')); ?>
		<?php echo $form->error($model,'game_id'); ?>
	</div>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

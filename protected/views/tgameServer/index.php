<?php
/* @var $this TgameServerController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle=Yii::app()->name . " - Топ игровых серверов";
?>

<h1 class='page-header'>Топ игровых серверов</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (count($modelGame) >= 1):?>
<table class="table table-condensed table-hover table-clickable chip-offers">
    <thead>
    <tr>
        <th width="90%">название игры</th>
    </tr>
    </thead>
    <tbody>
        <?foreach($modelGame as $modelAlert):?>
            <tr data-href="<?=$this->createUrl('tgameServer/top', array('id'=>$modelAlert->id))?>">
                <td>
                    <div class="text-left" style="float:left;"><?=$modelAlert->name?></div>
                    <div class="text-right">Серверов сайта: <strong><?=count($modelAlert->serv)?></strong> (<strong>+ <?=count($modelAlert->tgame)?></strong> пользовательских)</div>
                </td>
            </tr>
        <?endforeach?>
    </tbody>
</table>
<?$this->widget('CLinkPager', array(
    'header' => '',
    'pages' => $pages,
    'htmlOptions'=> array('class'=>'pagination'),
));?>
<?else:?>
<div class="alert alert-info">
    <strong>Извините, но игры отсутствуют =(</strong>
</div>
<?endif?>

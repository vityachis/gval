<?php
$this->pageTitle=Yii::app()->name . " - Топ серверов " . $modelGame->name;
$userGuest = Yii::app()->user->isGuest;
?>

<h1 class="page-header">Топ серверов <?=$modelGame->name?>
    <a href="<?=$this->createUrl('/tgameServer/create')?>"><button type="button" class="btn btn-info">Создать новый сервер</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<div id="yw0" class="tabbable">
    <ul class="nav nav-tabs" role="menu">
        <li class="active" role="menuitem">
            <a aria-expanded="true" data-toggle="tab" tabindex="-1" href="#tab_1">Сервера сайта</a>
        </li>
        <li class="" role="menuitem">
            <a aria-expanded="false" data-toggle="tab" tabindex="-1" href="#tab_2">Пользовательские сервера</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="tab_1" class="tab-pane fade active in"><br>
            <?if (count($modelServerSite) >= 1):?>
            <div class="alert alert-info" style="text-align:center;">Рейтинг серверов сайта поднимается за счет <strong>успешных сделок</strong>!</div>
            <table class="table table-condensed table-hover table-clickable chip-offers">
                <thead>
                <tr>
                    <th width="10%">№ в топе</th>
                    <th width="80%">название сервера</th>
                    <th width="10%">рейтинг</th>
                </tr>
                </thead>
                <tbody>
                <?$iCount = 1;
                if (isset($_GET['page'])) {
                    $page = (int) $_GET['page'];
                    $iCount += $page * 25;
                }?>
                    <?foreach($modelServerSite as $modelServerSiteAlert):?>
                        <tr>
                            <td width="10%"><div><?=$iCount++?></div></td>
                            <td width="80%"><div><?=$modelServerSiteAlert->name?></div></td>
                            <td width="10%"><div><?=count($modelServerSiteAlert->goods)?></div></td>
                        </tr>
                    <?endforeach?>
                </tbody>
            </table>
            <?$this->widget('CLinkPager', array(
                'header' => '',
                'pages' => $pagesSite,
                'htmlOptions'=> array('class'=>'pagination'),
            ));?>
            <?else:?>
            <div class="alert alert-info">
                <strong>Извините, но серверов сайта нет для данной игры =(</strong>
            </div>
            <?endif?>
        </div>

        <div id="tab_2" class="tab-pane fade"><br>
            <?if (count($modelServerCustom) >= 1):?>
            <div class="alert alert-info" style="text-align:center;">Рейтинг серверов пользователей поднимается за счет <strong>поднятия рейтинга пользователями</strong>!</div>
            <span class="help-block"><strong>Внимание!</strong> Поднятие рейтинга стоит <?=WorkView::pricesService('serverUp')?>$ за единицу.</span>
            <table class="table table-condensed table-hover table-clickable chip-offers">
                <thead>
                <tr>
                    <th>№ в топе</th>
                    <th>название сервера</th>
                    <th class="text-center">создатель</th>
                    <th class="text-center">ссылка</th>
                    <th class="text-right">рейтинг</th>
                    <?if (!$userGuest):?>
                        <th class="text-right">поднять рейтинг</th>
                    <?endif?>
                </tr>
                </thead>
                <tbody>
                <?$iCount = 1;
                if (isset($_GET['page'])) {
                    $page = (int) $_GET['page'];
                    $iCount += $page * 25;
                }?>
                    <?foreach($modelServerCustom as $modelServerCustomAlert):?>
                    <?if (!$userGuest):?>
                        <tr data-href="<?=$this->createUrl('tgameServer/topUp', array('id'=>$modelServerCustomAlert->id, 'up'=>'custom', 'game'=>$modelGame->id))?>">
                    <?else:?>
                        <tr>
                    <?endif?>
                            <td><div><?=$iCount++?></div></td>
                            <td><div><?=$modelServerCustomAlert->name?></div></td>
                            <td class="text-center"><div><?=$modelServerCustomAlert->user->nick?></div></td>
                            <td class="text-center"><div><a href="<?=$modelServerCustomAlert->href?>" target="_blank"><?=$modelServerCustomAlert->href?></a></div></td>
                            <td class="text-right"><div><?=$modelServerCustomAlert->ret?></div></td>
                            <?if (!$userGuest):?>
                                <td class="text-right"><div><span class="glyphicon glyphicon-upload"></span></div></td>
                            <?endif?>
                        </tr>
                    <?endforeach?>
                </tbody>
            </table>
            <?$this->widget('CLinkPager', array(
                'header' => '',
                'pages' => $pagesCustom,
                'htmlOptions'=> array('class'=>'pagination'),
            ));?>
            <?else:?>
            <div class="alert alert-info">
                <strong>Извините, но пользовательских серверов нет для данной игры =(</strong>
            </div>
            <?endif?>
        </div>
    </div>
</div>



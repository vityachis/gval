<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */

$this->breadcrumbs=array(
	'Tgame Servers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TgameServer', 'url'=>array('index')),
	array('label'=>'Create TgameServer', 'url'=>array('create')),
	array('label'=>'View TgameServer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TgameServer', 'url'=>array('admin')),
);
?>

<h1>Update TgameServer <?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

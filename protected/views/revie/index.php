<?php
/* @var $this RevieController */

$this->pageTitle = Yii::app()->name . ' - Список отзывов о Вас';
?>

<h1 class="page-header">Список отзывов о Вас</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'rowCssClassExpression'=>function($row,$data){if($data->ret==1){return 'success';}else{return 'danger';}},
    'id'=>'goods-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        array('name'=>'userNick',
            'type'=>'html',
            'value'=>'CHtml::link($data->user->nick, array(\'/users/index\', \'id\'=>$data->user->id))',
        ),
        array('name'=>'ret',
            'type'=>'html',
            'value'=>function($data){if($data->ret==1){return "Положительный";}else{return "Отрицательный";}},
        ),
        'name'=>'text',
    ),
)); ?>

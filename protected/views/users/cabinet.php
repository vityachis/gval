<?php
/* @var $this UsersController */

$this->pageTitle = Yii::app()->name . ' - Ваша анкета';
?>

<h1 class='page-header'>Ваша анкета
	<?if (Yii::app()->user->id === $model->id):?>
		<a href="<?=$this->createUrl('users/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
	<?endif?>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if ($model->ban !== '0') {
	$banValue = 'Забанен до ' . date('Y.m.d H:i:s', $model->ban_time + $model->lvl_ban);
	} else {$banValue = 'Отсутствует';}?>
<?if ($model->online !== '0') {$onValue = 'Сейчас в сети';} else {$onValue = date('d.m.Y H:i:s', $model->date_last);}?>
<?if ($model->pol == '1') {$polValue = 'Мужской';} else if ($model->pol == '2') {$polValue = 'Женский';} else {$polValue = 'Ошибка!';}?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		'nick',
		'name',
		'surname',
		'patron',
		array('name'=>'pol', 'value'=>$polValue),
		'email',
		// 'pass',
		'icq',
		'skype',
		array('name'=>'date_reg', 'value'=>date('d.m.Y H:i:s', $model->date_reg)),
	),
)); ?>

<?php
/* @var $this UsersController */
/* @var $model Users */
?>

<h1 class="page-header">Редактировать свой профиль</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

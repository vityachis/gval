<?php
/* @var $this UsersController */

$this->pageTitle = Yii::app()->name . ' - Пользователь ' . $model->name . ' (id' . $model->id . ')';
?>

<h1 class='page-header'><?if ($model->id !== '777') echo "Пользователь"?> <?=$model->name?> (id<?=$model->id?>)
	<?if (Yii::app()->user->id !== $model->id && $model->id !== '777'):?>
		<a href="<?=$this->createUrl('mail/index', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Написать сообщение</button></a>
	<?endif?>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
<?if ($model->id == 777):?>
	<div class="alert alert-danger" style="text-align:center;">
		<strong>Вы не можете просмотреть профиль Системы!</strong>
	</div>
<?else:?>
<?if ($model->ban !== '0') {
	$banValue = 'Забанен до ' . date('Y.m.d H:i:s', $model->ban_time + $model->lvl_ban);
	} else {$banValue = 'Отсутствует';}?>
<?if ($model->online !== '0') {$onValue = 'Сейчас в сети';} else {$onValue = date('d.m.Y H:i:s', $model->date_last);}?>
<?if ($model->pol == '1') {$polValue = 'Мужской';} else if ($model->pol == '2') {$polValue = 'Женский';} else {$polValue = 'Ошибка!';}?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		'nick',
		'name',
		'surname',
		'patron',
		array('name'=>'pol', 'value'=>$polValue),
		'email',
		// 'pass',
		array('name'=>'ban', 'value'=>$banValue),
		'icq',
		'skype',
		array('name'=>'date_reg', 'value'=>date('d.m.Y H:i:s', $model->date_reg)),
		array('name'=>'date_last', 'value'=>$onValue),
	),
)); ?>

<?if ($revieModel->isNewRecord || isset($_GET['edit'])):?>
	<div class="form">

	<?php $form=$this->beginWidget('TbActiveForm', array(
		'id'=>'revie-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		'htmlOptions' => array('class'=>'form-narrow'),
	)); ?>

		<h1 class="page-header">Оставить отзыв</h1>

		<div class="form-group">
			<?php echo $form->labelEx($revieModel,'ret'); ?>
			<?php echo $form->dropDownList($revieModel, 'ret', array(1=>'Положительный (+)', 0=>'Отрицательный (-)'));?>
			<?php echo $form->error($revieModel,'ret'); ?>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($revieModel,'text'); ?>
			<?php echo $form->textArea($revieModel,'text'); ?>
			<?php echo $form->error($revieModel,'text'); ?>
		</div>

		<?$revieModel->from_id = $model->id?>

		<div class="form-group buttons">
			<?php echo TbHtml::submitButton('Оставить отзыв'); ?>
		</div>

	<?php $this->endWidget(); ?>

	</div><!-- form -->
<?endif?>

<div class="revie-list">
	<h1 class="page-header">Список отзывов
		<?if (!$revieModel->isNewRecord && !isset($_GET['edit'])):?>
			<a href="<?=$this->createUrl('users/index', array('id'=>$model->id, 'edit'=>null))?>"><button type="button" class="btn btn-info">Редактировать свой отзыв</button></a>
		<?endif?>
	</h1>
	<table class="table table-striped table-bordered">
		<tbody>

				<?if (count($revieModelAll) >= 1):?>
					<?foreach($revieModelAll as $revieModelOne):?>
						<?if ($revieModelOne->user_id !== Yii::app()->user->id) {
							$nickRevie = "<a href='".$this->createUrl('users/index', array('id'=>$revieModelOne->user_id))."'>".$revieModelOne->user->nick."</a>";
						} else {
							$nickRevie = $revieModelOne->user->nick;
						}

						if ($revieModelOne->ret == 1) $classRet = 'success';
						else $classRet = 'danger'?>

						<tr class="<?=$classRet?>">
							<th width="10%"><?=$nickRevie?></th>
							<td width="15%"><?=$revieModelOne->ret?"Положительный":"Отрицательный"?></td>
							<td width="75%"><?=$revieModelOne->text?></td>
						</tr>
					<?endforeach?>
				<?else:?>
					<tr>
						<th class="text-center">Отзывы отсутствуют</th>
					</tr>
				<?endif?>

		</tbody>
	</table>
	<?$this->widget('CLinkPager', array(
		'header' => '',
	    'pages' => $pages,
	    'htmlOptions'=> array('class'=>'pagination'),
	));?>
</div>
<?endif?>


<?php
/* @var $this GoodsController */

$this->pageTitle = Yii::app()->name . ' - Список ваших товаров ';
?>

<h1 class="page-header">Список ваших товаров
    <a href="<?=$this->createUrl('/goods/create')?>"><button type="button" class="btn btn-info">Создать новый товар</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (count($model) >= 1):?>
<table class="table table-condensed table-hover table-clickable chip-offers">
    <thead>
    <tr>
        <th>сервер</th>
        <th>сторона</th>
        <th>игра</th>
        <th class="text-center">наличие</th>
        <th class="text-center">цена</th>
        <th class="text-right">название</th>
        <th class="text-right">тип товара</th>
        <th class="text-right">статус</th>
    </tr>
    </thead>
    <tbody>
        <?foreach($model as $modelAlert):?>
        <?$orderGoodModel = $modelOrder[$modelAlert->id]['count']?>
        <?if ($orderGoodModel == 1) {
            $addrTabl = 'goods/cancel';
            $idTabl = $modelOrder[$modelAlert->id]['id'];
        } else {
            $addrTabl = 'goods/update';
            $idTabl = $modelAlert->id;
        }
        ?>
            <tr data-href="<?=$this->createUrl($addrTabl, array('id'=>$idTabl))?>">
                <td><?=$modelAlert->serv->name?></td>
                <td><?=$modelAlert->part->name?></td>
                <td>
                    <div><?=$modelAlert->game->name?></div>
                </td>
                <td class="text-center"><?=$modelAlert->count?> единиц</td>
                <td class="text-center">
                    <div><?=$modelAlert->cost * $modelAlert->count?> $</div>
                </td>
                <td class="text-right">
                    <div><?=$modelAlert->name?></div>
                </td>
                <td class="text-right">
                    <div><?=$modelAlert->type->name?></div>
                </td>
                <td class="text-right">
                <?if ($orderGoodModel == 1):?>
                    <span class="label label-danger">Покупатель <?=$modelUsers[$modelOrder[$modelAlert->id]['user']]['nick']?></span>
                <?else:?>
                    <span class="label label-success">Продается</span>
                <?endif?>
                </td>
                <td>

                </td>
            </tr>
        <?endforeach?>
    </tbody>
</table>
<?$this->widget('CLinkPager', array(
    'header' => '',
    'pages' => $pages,
    'htmlOptions'=> array('class'=>'pagination'),
));?>
<?else:?>
<div class="alert alert-info">
    <strong>Извините, но в данной категории нет товаров =(</strong>
</div>
<?endif?>

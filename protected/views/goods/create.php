<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle = Yii::app()->name . ' - Создать новый товар';
?>

<h1 class="page-header">Создать новый товар</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownType'=>$dropDownType,
    'dropDownGame'=>$dropDownGame,
    'dropDownServ'=>$dropDownServ,
    'dropDownPart'=>$dropDownPart,
)); ?>

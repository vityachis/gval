<?php
/* @var $this GoodsController */

$this->pageTitle = Yii::app()->name . ' - Отмена сделки';
?>

<div class="alert alert-warning in alert-block fade">
    <h4>Отмена сделки!</h4>
    <p>Вы действительно желаете отменить Вашу сделку с пользователем <a href="<?=$this->createUrl('users/index',array('id'=>$model->id_user))?>"><?=$modelUsers->nick?></a> (<?=$modelUsers->name?>) ?</p>
    <p><strong>Детали сделки:</strong></p>
    <ul>
        <li><strong>Название: </strong><?=$modelGoods->name?></li>
        <li><strong>Игра: </strong><?=$modelGoods->game->name?></li>
        <li><strong>Сервер: </strong><?=$modelGoods->serv->name?></li>
        <li><strong>Сторона: </strong><?=$modelGoods->part->name?></li>
        <li><strong>Тип товара: </strong><?=$modelGoods->type->name?></li>
        <li><strong>Количество: </strong><?=$model->good->count?> единиц</li>
        <li><strong>Цена: </strong><?=$model->good->count * $model->good->cost?></li>
    </ul>
    <div style="margin-top:10px;">
        <a href="<?=$this->createUrl('order/cancel', array('id'=>$model->id))?>"><button class="btn btn-warning" type="button">Отменить сделку</button></a>
        <a href="<?=$this->createUrl('mail/index', array('id'=>$model->id_user))?>"><button class="btn btn-info" type="button">Написать пользователю</button></a>
    </div>
</div>

<?php
/* @var $this GoodsController */

$this->pageTitle = Yii::app()->name . ' - Купить ' . $model->name;
?>
<h1 class="page-header">Покупка товара: <?=$model->name?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php
	if($model->user->online==1) $statusOn = '<span class="label label-success">online</span>';
	if($model->user->online==0) $statusOn = '<span class="label label-danger">offline</span>';
?>

<div class="margin-bottom">
    <table class="values">
      	<tbody>
	      	<tr>
	        	<th>Продавец</th>
	        	<td><a href="<?=$this->createUrl('users/index', array('id'=>$model->user->id))?>"><?=$model->user->nick?></a>
	        	<?=$statusOn?>
	      	</tr>
      		<tr>
  				<th>Игра</th>
  				<td><?=$model->game->name?></td>
			</tr>
			<tr>
  				<th>Тип товара</th>
  				<td><?=$model->type->name?></td>
			</tr>
			<tr>
			  	<th>Сервер</th>
			  	<td><?=$model->serv->name?></td>
			</tr>
			<tr>
        		<th>Наличие</th>
        		<td><?=$model->count?> единиц</td>
      		</tr>
      		<tr>
      		  	<th>Название товара</th>
      		  	<td><?=$model->name?></td>
      		</tr>
      		<tr>
      		  	<th>Цена</th>
      		  	<td><?=$model->cost * $model->count?> $<span style="color:red;">*</span></td>
      		</tr>
    	</tbody>
    </table>
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
  'id'=>'goods-form',
  'enableAjaxValidation'=>false,
  'htmlOptions' => array('class'=>'form-narrow'),
)); ?>

<div class="form-group">
  <?php echo $form->hiddenField($buyGoods,'status'); ?>
  <?php $this->widget('bootstrap.widgets.TbModal', array(
      'id' => 'myModal',
      'header' => 'Покупка товара',
      'content' => '<h5>Если вы купите данный товар, с вашего счета автоматический будет снято <span style="color:red;">' . $model->cost*$model->count . '$</span> в систему!</h5>',
      'footer' => array(
          TbHtml::submitButton('Да, я хочу купить этот товар', array('goods-id'=>$model->id, 'id'=>'buyGoods', 'data-dismiss' => 'modal', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)),
          TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
       ),
  )); ?>

  <?php echo TbHtml::button('Купить', array(
      'data-toggle' => 'modal',
      'data-target' => '#myModal',
  )); ?>
  <a href='<?=$this->createUrl('mail/index', array('id'=>$model->user->id))?>'>
    <?php echo TbHtml::button('Перейти в чат', array('color' => TbHtml::BUTTON_COLOR_INFO))?>
  </a>
</div>
<span class="help-block" style="font-size:10px;">* Так же будет снята плата в размере <?=WorkView::pricesPurch('percentPurch')?>% от сумы покупки! <?=WorkView::pricesPurch('percentPurch')?>% от данной покупки, равно <?=(($model->cost * $model->count) * WorkView::pricesPurch('percentPurch')) / 100?> $</span>

<?php $this->endWidget(); ?>

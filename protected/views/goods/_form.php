<?php
/* @var $this GoodsController */
/* @var $model Goods */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'goods-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('class'=>'form-narrow'),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> заполнять обязательно.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'type_good'); ?>
		<?php echo $form->dropDownList($model, 'type_good', TbHtml::listData($dropDownType, 'id', 'name'));?>
		<?php echo $form->error($model,'type_good'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'game_id'); ?>
		<?php echo $form->dropDownList($model, 'game_id', TbHtml::listData($dropDownGame, 'id', 'name')); ?>
		<?php echo $form->error($model,'game_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'serv_id'); ?>
		<?echo $form->dropDownList($model, 'serv_id', TbHtml::listData($dropDownServ, 'id', 'name'));?>
		<?php echo $form->error($model,'serv_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'part_id'); ?>
		<?echo $form->dropDownList($model,'part_id', TbHtml::listData($dropDownPart, 'id', 'name'));?>
		<?php echo $form->error($model,'part_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'count'); ?>
		<?php echo $form->textField($model,'count'); ?>
		<?php echo $form->error($model,'count'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
/* Подгрузка сервера */
jQuery('body').on('change','#Goods_game_id',function(){jQuery.ajax({'type':'GET','url':'/Goods/ajaxDropDownListPart','data':{'id':this.value},'cache':false,'success':function(html){jQuery("#Goods_part_id").html(html)}});return false;});
/* Подгрузка стороны */
jQuery('body').on('change','#Goods_game_id',function(){jQuery.ajax({'type':'GET','url':'/Goods/ajaxDropDownListServ','data':{'id':this.value},'cache':false,'success':function(html){jQuery("#Goods_serv_id").html(html)}});return false;});
</script>

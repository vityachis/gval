<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle = Yii::app()->name . ' - Редактировать товар ' . $model->name;
?>

<h1 class="page-header">Редактировать товар: <?php echo $model->name; ?> [<?=$model->id?>]</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownType'=>$dropDownType,
    'dropDownGame'=>$dropDownGame,
    'dropDownServ'=>$dropDownServ,
    'dropDownPart'=>$dropDownPart,
)); ?>

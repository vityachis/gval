<?php
/* @var $this GameController */

$this->pageTitle = Yii::app()->name . ' - Список товаров ' . $infoGame->name;
?>

<h1 class="page-header">Предметы <?=$infoGame->name?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'goods-grid',
	'dataProvider'=>$model->search($infoGame->id),
	'columns'=>array(
		array('name'=>'name',
			'type'=>'html',
			'value'=>'CHtml::link($data->name, array(\'/goods/index\', \'id\'=>$data->id))',
		),
		array(
			'name'=> 'typeName',
			'value'=>'$data->type->name',
		),
		array(
			'name'=> 'userNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->user->nick, array(\'/users/index\', \'id\'=>$data->user->id))',
		),
		'count',
		array(
			'name'=>'cost',
			'value'=> function($data){return $data->count*$data->cost . "$";},
		),
		array(
			'name'=> 'servName',
			'value'=>'$data->serv->name',
		),
		array(
			'name'=> 'partName',
			'value'=>'$data->part->name',
		),
	),
)); ?>

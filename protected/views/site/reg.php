<?php
$this->pageTitle=Yii::app()->name . ' - Регистрация';
?>

<div class="photo-titl">Регистрация</div>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (!Yii::app()->user->isGuest) {
    $this->redirect(array('index'));
    exit;
}?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
    'id'=>'users-form',
    'enableAjaxValidation'=>true,
    'htmlOptions' => array('class'=>'form-narrow'),
)); ?>

	<p>Пожалуйста, заполните следующую форму для регистрации нового аккаунта:</p>
    <p class="note">Поля отмеченные <span class="required">*</span> обязательно заполнять!</p>

    <div class="form-group">
        <?php echo $form->labelEx($model,'nick'); ?>
        <?php echo $form->textField($model,'nick'); ?>
        <span class="help-block">Латинские буквы и цифры</span>
        <?php echo $form->error($model,'nick'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <span class="help-block">Кириллические буквы</span>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'pol'); ?>
        <select name="Users[pol]" id="Users_pol" class="form-control">
            <option value="1">Мужской</option>
            <option value="0">Женский</option>
        </select>
        <?php echo $form->error($model,'pol'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'pass'); ?>
        <?php echo $form->passwordField($model,'pass'); ?>
        <span class="help-block">Минимум 6 символов</span>
        <?php echo $form->error($model,'pass'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <?php if(CCaptcha::checkRequirements()): ?>
    <div class="form-group">
        <?php $this->widget('CCaptcha', array(
            'clickableImage' => true,
            'buttonLabel' => '<span class="glyphicon glyphicon-refresh"></span>',
        )); ?>
        <br />
    	<?php echo $form->labelEx($model,'verifyCode'); ?>
    	<div>
    	<?php echo $form->textField($model,'verifyCode'); ?>
    	</div>
    	<div class="help-block">Пожалуйста введите буквы, изображенные на картинке выше.
    	<br/>Буквы не чуствительны к регистру.</div>
    	<?php echo $form->error($model,'verifyCode'); ?>
    </div>
    <?php endif; ?>

    <?=TbHtml::button('Зарегистрироваться', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => 'submit'))?>

<?php $this->endWidget(); ?>

</div><!-- form -->

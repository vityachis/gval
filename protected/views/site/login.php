<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
?>

<h1 class='page-header'>Вход</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>Пожалуйста, заполните следующую форму с вашими учетными данными для входа:</p>

<div class="form">
<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'login-form',
	'htmlOptions' => array('class'=>'form-narrow'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<?//=CPasswordHelper::hashPassword('admin')?>
	<p class="note">Поля отмеченные <span class="required">*</span> заполнить обязательно.</p>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nick'); ?>
		<?php echo $form->textField($model,'nick'); ?>
		<?php echo $form->error($model,'nick'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pass'); ?>
		<?php echo $form->passwordField($model,'pass'); ?>
		<?php echo $form->error($model,'pass'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
	</div>

	<?=TbHtml::button('Войти', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'type' => 'submit'))?>

<?php $this->endWidget(); ?>
</div><!-- form -->

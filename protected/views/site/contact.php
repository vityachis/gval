<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Контакты';
?>

<h1 class='page-header'>Контакты</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Если у вас есть деловое предложение или другие вопросы, пожалуйста, заполните следующую форму, чтобы связаться с нами. Спасибо.
</p>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array(
		'class' => 'form-narrow',
	),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> заполнять обязательно.</p>
	<?$disabledInput = array();
	if (!Yii::app()->user->isGuest) {
		$model->name = $modelUser->name;
		$model->email = $modelUser->email;
		$disabledInput = array('disabled'=>true);
	}
	?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name', $disabledInput); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email', $disabledInput); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject', array('size'=>60,'maxlength'=>128, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body', array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="form-group">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha', array(
		    'clickableImage' => true,
		    'buttonLabel' => '<span class="glyphicon glyphicon-refresh"></span>',
		)); ?>
		<?php echo $form->textField($model,'verifyCode', array('class'=>'form-control')); ?>
		</div>
		<div class="hint">Пожалуйста, введите буквы, изображенные на картинке выше.
		<br />Буквы не чувствительны к регистру.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton('Отправить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

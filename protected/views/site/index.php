<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<h1 class="page-header image">Покупайте у игроков!</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<ul class="games">
	<?foreach($gameList as $gameListAlert):?>

	<?// Текст количества товаров
	if (count($gameListAlert->goodStatus0) == 1) $textGoods = 'предложение';
	else if (count($gameListAlert->goodStatus0) >= 2 && count($gameListAlert->goodStatus0) <= 4) $textGoods = 'предложения';
	else $textGoods = 'предложений';

	// Текст количества заказов
	if (count($gameListAlert->goodStatus1) == 1) $textOrders = 'заказ';
	else if (count($gameListAlert->goodStatus1) >= 2 && count($gameListAlert->goodStatus1) <= 4) $textOrders = 'заказа';
	else $textOrders = 'заказов';?>
		<li>
			<div class="content">
		        <div class="logo" style="background:url(/images/game_icon/<?=WorkView::imageGame($gameListAlert->id)?>.jpg) no-repeat 0 0;"></div>
		        <div class="info">
		        	<div class="name"><?=$gameListAlert->name?></div>
		            <!-- <div class="price">от 0.61 ₽ за 1 кк</div> -->
		            <div class="amount"><?=count($gameListAlert->goodStatus0).' '.$textGoods?> (+ <?=count($gameListAlert->goodStatus1).' '.$textOrders?>)</div>
		        </div>
		    </div>
		    <?=CHtml::link('', array('game/index/', 'id'=>$gameListAlert->id), array('class'=>'block'))?>
	    </li>
	<?endforeach?>
</ul>
<?$this->widget('CLinkPager', array(
	'header' => '',
    'pages' => $pages,
    'htmlOptions'=> array('class'=>'pagination'),
));?>

<?php /* @var $this Controller */?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="ru">
	<?php Yii::app()->bootstrap->register(); ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/css/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/myStyle.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</head>

<body>
<div class="container" id="page">
<?php
if (WorkView::countCustomServer(INTEGER_PRINT) >= 1 && $this->myInfoNick == 'admin') {
	$topLinkBar = array(
			'label' => 'Топ игровых серверов '.WorkView::countCustomServer(),
			'encodeLabel'=>false,
			    'items' => array(
						    	array('label' => 'Список игровых серверов', 'url' => '/tgameServer/index'),
						    	array('label' => 'Модерация '.WorkView::countCustomServer(), 'url' => '/tgameServer/moder'),
							),
			    );
} else {
	$topLinkBar = array('label' => 'Топ игровых серверов', 'url' => '/tgameServer/index');
}
?>

<?if (!Yii::app()->user->isGuest) {
	$itemsLeftMenu = array(
		array('label' => 'Сообщения '.WorkView::countNewMail(), 'url' => '/mail/cabinet', 'type'=>'raw'),
		array(
			'label' => WorkView::countOrderGoodsRevie(),
			'encodeLabel'=>false,
		    'items' => array(
							array('label' => 'Заказы '.WorkView::countNewOrder(), 'url' => '/order/cabinet'),
							array('label' => 'Продажа '.WorkView::countNewGoods(), 'url' => '/goods/cabinet'),
							array('label' => 'Отзывы '.WorkView::countNewRevie(), 'url' => '/revie/index'),
						),
		),
		$topLinkBar,
		array(
			'label' => 'О сайте',
				'encodeLabel'=>false,
			    'items' => array(
			    				array('label' => 'Правила для продавцов', 'url' => '/goods/infoSaller'),
						    	array('label' => 'Контакты', 'url' => '/site/contact'),
								array('label' => 'Помощь', 'url' => '/site/page?view=about'),

							),
		),
);} else {
	$itemsLeftMenu = array(
		array('label' => 'Помощь', 'url' => '/site/page?view=about'),
		array('label' => 'Контакты', 'url' => '/site/contact'),
		array('label' => 'Топ игровых серверов', 'url' => '/tgameServer/index'),
);}?>
<?if (Yii::app()->user->isGuest) {
	$itemsAuth = array(
		array('label' => 'Войти', 'url' => '/site/login'),
		array('label' => 'Зарегистрироваться', 'url' => '/site/reg'),
	);
} else if (Yii::app()->user->name == 'admin') {
	$itemsAuth = array(
		array('label' => Yii::app()->user->name, 'url' => '/users/cabinet'),
		array('label' => 'Панель управления', 'url' => '/backend', 'id'=>'adm-site'),
		array('label' => "<span class='glyphicon glyphicon-log-out'></span>", 'url' => '/site/logout', 'type'=>'raw'),
	);
} else {
	$itemsAuth = array(
		array('label' => Yii::app()->user->name, 'url' => '/users/cabinet'),
		array('label' => "<span class='glyphicon glyphicon-log-out'></span>", 'url' => '/site/logout', 'type'=>'raw'),
	);
}?>
	<div id="header">
		<?php $this->widget('bootstrap.widgets.TbNavbar', array(
		    'brandLabel' => Yii::app()->name,
		    'htmlOptions' => array('style'=>'border-radius:0px;'),
		    'display' => null, // default is static to top
		    'collapse' => true,
		    'items' => array(
		        array(
		        	'encodeLabel'=>false,
		            'class' => 'bootstrap.widgets.TbNav',
		            'items' => $itemsLeftMenu,
		        ),
		        array(
		            'class' => 'bootstrap.widgets.TbNav',
		            'encodeLabel'=>false,
		            'htmlOptions' => array('class'=>'navbar-right'),
		            'items' => $itemsAuth,
		        ),
		    ),
		)); ?>
	</div><!-- header -->

	<?if (!Yii::app()->user->isGuest):?>
	<div class="container">
		<div class="alert alert-warning in alert-block fade">
			<table class="chip-offers" style="width:100%;">
				<tr>
					<th class="text-left">На вашем счету: <?=WorkView::myCurren()?>
					<a style="margin-left:20px;text-decoration:none;" href="<?=$this->createUrl('users/fillUpPurse')?>">Пополнить счет?</a>
					<a style="margin-left:20px;text-decoration:none;" href="<?=$this->createUrl('users/fillDownPurse')?>">Вывести средства</a></th>
					<th class="text-right">Денег в <a href="<?=$this->createUrl('order/cabinet')?>">корзине</a>: <?=WorkView::myCurrenBasket()?></th>
				</tr>
			</table>
		</div>
	</div>
	<?endif?>

	<?$textPageCont = WorkView::textPage('/');
	foreach($textPageCont as $value) {
		$textPageArr[$value->id] = $value->full_text;
	}?>

	<div id="content" class="container main-container content-chips content-chips-promo">

		<!-- # path='/' && id='1' # -->
		<div class='text-block callout callout-info'>
			<p>
				<?=$textPageArr['1']?>
			</p>
		</div>
		<!-- END # path='/' && id='1' # END -->

		<?php echo $content; ?>

		<div class="clear"></div>

	</div>

	<footer class="main-footer">
		<div class="container">
		    <ul class="right">
		    	<li>© <?=date('Y')?> GameVal</li>
		    	<li>Обратная связь: <a href="mailto:vityachis@ya.ru">vityachis@ya.ru</a></li>
		    </ul>
		</div>
	</footer>

</div><!-- page -->

</body>
</html>

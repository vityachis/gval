<?php
/* @var $this MailController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle=Yii::app()->name . ' - История сообщений';
?>

<h1 class='page-header'>История сообщений</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if (count($model) >=1):?>
    <?foreach ($model as $mail) {
        if ($mail->to_id == Yii::app()->user->id) {
            $id = $mail->from_id;
        }
        else if ($mail->from_id == Yii::app()->user->id) {
            $id = $mail->to_id;
        }
        $comunication[$id] = $userInfoMail[$id];
    }
    ?>
    <?foreach ($comunication as $key => $value):?>

    <?if ($countNewMail[$key] >= 1) {
        $newMailUserTo = '<span style="float:right;" class="label label-danger">' . $countNewMail[$key] . '</span>';
    } else {$newMailUserTo = null;}
    if ($key == '777') {
        $value = '<strong>' . $value . '</strong>';
    }
    ?>
        <a href="<?=$this->createUrl('mail/index', array('id'=>$key))?>" class="list-group-item"><?=$value?> <?=$newMailUserTo?></a>
    <?endforeach?>
<?else:?>
    <div class="alert alert-info">
        <strong>Извините, но диалоги отсутствуют =(</strong>
    </div>
<?endif?>

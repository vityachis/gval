<?php
/* @var $this MailController */
/* @var $model Mail */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'mail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('class'=>'form-narrow'),
)); ?>
	<?if ($infoSomeone->id == 777):?>

		<div class="alert alert-danger" style="text-align:center;">
			<strong>Вы не можете писать системе!</strong>
		</div>

	<?else:?>

		<?php echo $form->errorSummary($model); ?>

		<div class="form-group">
			<?php echo $form->textArea($model,'text_mail',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'text_mail'); ?>
		</div>

		<div class="form-group">
			<?php echo $form->hiddenField($model,'from_id', array('value'=>$infoSomeone->id)); ?>
		</div>

		<div class="form-group buttons">
			<?php echo TbHtml::submitButton('Отправить'); ?>
		</div>

	<?endif?>

<?php $this->endWidget(); ?>

</div><!-- form -->

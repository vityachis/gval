<?php
/* @var $this MailController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle=Yii::app()->name . ' - Диалог';
?>

<h1 class='page-header'>Диалог</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'infoSomeone'=>$infoSomeone)); ?>

<?if (count($modelComunication) >= 1):?>
    <?foreach ($modelComunication as $oneMail):?>
        <?if ($oneMail->to_id == Yii::app()->user->id) {
            $oneName = $infoMy->nick;
            $alertClass = 'alert-warning';
        } else {
            if ($oneMail->to_id == '777') {
                $oneName = $infoSomeone->nick;
            } else {
                $oneName = '<a style="text-decoration:none;" href="' . $this->createUrl('users/index', array('id'=>$infoSomeone->id)) . '">' . $infoSomeone->nick . '</a>';
            }
            $alertClass = 'alert-info';
        }?>
        <div class="alert <?=$alertClass?>" role="alert" style='margin-bottom: 5px;'>
            <b><?=$oneName?></b> [<?=date('d.m.Y H:i:s', $oneMail->send_date)?>] : <?=$oneMail->text_mail?>
            <?if ($oneMail->status == 4):?>
                <span class="glyphicon glyphicon-record" style="color:red;float:right;"></span>
            <?endif?>
        </div>
    <?endforeach?>
    <?$this->widget('CLinkPager', array(
        'header' => '',
        'pages' => $pages,
        'htmlOptions'=> array('class'=>'pagination'),
    ));?>
<?else:?>
    <div class="alert alert-info">
        <strong>Извините, но сообщения отсутствуют =(</strong>
    </div>
<?endif?>

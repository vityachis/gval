<?php

/**
 * This is the model class for table "{{mail}}".
 *
 * The followings are the available columns in table '{{mail}}':
 * @property integer $id
 * @property integer $to_id
 * @property integer $from_id
 * @property integer $status
 * @property string $send_date
 * @property string $text_mail
 */
class Mail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{mail}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('to_id, from_id, status, send_date, text_mail', 'required'),
			array('to_id, from_id, status', 'numerical', 'integerOnly'=>true),
			array('send_date', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, to_id, from_id, status, send_date, text_mail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userDataFrom' => array(self::BELONGS_TO, 'Users', 'to_id'),
			'userDataTo' => array(self::BELONGS_TO, 'Users', 'from_id'),
			'userData' => array(self::BELONGS_TO, 'Users', 'from_id'),
			'userData' => array(self::BELONGS_TO, 'Users', 'to_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'to_id' => 'От кого',
			'from_id' => 'Для кого',
			'status' => 'Статус',
			'send_date' => 'Дата отправки',
			'text_mail' => 'Текст сообщения',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('to_id',$this->to_id);
		$criteria->compare('from_id',$this->from_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('send_date',$this->send_date,true);
		$criteria->compare('text_mail',$this->text_mail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() {
		if (parent::beforeSave())
		{
			if ($this->isNewRecord) {
				$this->send_date = time();
				$this->status = 1;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "{{order}}".
 *
 * The followings are the available columns in table '{{order}}':
 * @property integer $id
 * @property integer $id_goods
 * @property integer $id_users
 * @property integer $status
 */
class Order extends CActiveRecord
{

	private $_goodName;
	private $_userNick;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, status', 'required'),
			array('id_good, id_user, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_good, id_user, status, goodName, userNick', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'good' => array(self::BELONGS_TO, 'Goods', 'id_good'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_good' => 'Товар заказа',
			'id_user' => 'Покупатель',
			'status' => 'Статус',
			'goodName' => 'Товар заказа',
			'userNick' => 'Покупатель',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user', 'good');

		$criteria->compare('id',$this->id);
		$criteria->compare('id_good',$this->id_good);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('status',$this->status);

		$criteria->compare('user.nick',$this->userNick,true);
		$criteria->compare('good.name',$this->goodName,true);

		$sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes['userNick'] = array('asc' => 'user.nick','desc' => 'user.nick desc');
        $sort->attributes['goodName'] = array('asc' => 'good.name','desc' => 'good.name desc');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25),
			'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	==============================================================
	============= Start: Поиск по связанным таблицам =============
	==============================================================
	*/

	/**
	 * Поле: user_id
	 */
	public function getUserNick() {
	    if ($this->_userNick === null && $this->userNick !== null) {
	        $this->_userNick = $this->user->nick;
	    }
	    return $this->_userNick;
	}
	public function setUserNick($value)
	{
	    $this->_userNick = $value;
	}

	/**
	 * Поле: type_good
	 */
	public function getgoodName() {
	    if ($this->_goodName === null && $this->goodName !== null) {
	        $this->_goodName = $this->good->name;
	    }
	    return $this->_goodName;
	}
	public function setgoodName($value)
	{
	    $this->_goodName = $value;
	}

	/*
	============================================================
	============= End: Поиск по связанным таблицам =============
	============================================================
	*/

}

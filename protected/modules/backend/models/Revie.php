<?php

/**
 * This is the model class for table "{{revie}}".
 *
 * The followings are the available columns in table '{{revie}}':
 * @property integer $id
 * @property integer $from_id
 * @property integer $user_id
 * @property string $ret
 * @property string $cr_date
 * @property string $text
 * @property string $status
 */
class Revie extends CActiveRecord
{

	private $_toNick;
	private $_fromNick;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{revie}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, ret, cr_date', 'required'),
			array('from_id, user_id', 'numerical', 'integerOnly'=>true),
			array('ret, status', 'length', 'max'=>1),
			array('cr_date', 'length', 'max'=>16),
			array('text', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_id, user_id, ret, cr_date, text, status, toNick, fromNick', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'from' => array(self::BELONGS_TO, 'Users', 'from_id'),
			'to' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_id' => 'Для',
			'user_id' => 'Оставил',
			'ret' => 'Рейтинг',
			'cr_date' => 'Дата создания',
			'text' => 'Текст',
			'status' => 'Статус',
			'fromNick' => 'Для',
			'toNick' => 'Оставил',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('from','to');

		$criteria->compare('id',$this->id);
		$criteria->compare('from_id',$this->from_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('ret',$this->ret,true);
		$criteria->compare('cr_date',$this->cr_date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('status',$this->status,true);

		$criteria->compare('to.nick',$this->toNick,true);
		$criteria->compare('from.nick',$this->fromNick,true);

		$sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes['toNick'] = array('asc' => 'to.nick','desc' => 'to.nick desc');
        $sort->attributes['fromNick'] = array('asc' => 'from.nick','desc' => 'from.nick desc');
        $sort->attributes['ret'] = array('asc' => 't.ret','desc' => 't.ret desc');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25,),
			'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Revie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	==============================================================
	============= Start: Поиск по связанным таблицам =============
	==============================================================
	*/

	/**
	 * Поле: user_id
	 */
	public function gettoNick() {
	    if ($this->_toNick === null && $this->toNick !== null) {
	        $this->_toNick = $this->to->nick;
	    }
	    return $this->_toNick;
	}
	public function settoNick($value)
	{
	    $this->_toNick = $value;
	}

	/**
	 * Поле: from_id
	 */
	public function getfromNick() {
	    if ($this->_fromNick === null && $this->fromNick !== null) {
	        $this->_fromNick = $this->from->nick;
	    }
	    return $this->_fromNick;
	}
	public function setfromNick($value)
	{
	    $this->_fromNick = $value;
	}

	/*
	============================================================
	============= End: Поиск по связанным таблицам =============
	============================================================
	*/
}

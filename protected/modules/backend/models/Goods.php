<?php

/**
 * This is the model class for table "{{goods}}".
 *
 * The followings are the available columns in table '{{goods}}':
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property integer $type_good
 * @property integer $serv_id
 * @property integer $part_id
 * @property integer $game_id
 * @property integer $count
 * @property double $cost
 * @property integer $status
 * @property string $cr_date
 */
class Goods extends CActiveRecord
{

	private $_userNick;
	private $_typeGoodName;
	private $_gameName;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{goods}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, type_good, game_id, count, cost', 'required'),
			array('user_id, type_good, serv_id, part_id, game_id, count, status', 'numerical', 'integerOnly'=>true),
			array('cost', 'numerical'),
			array('name', 'length', 'max'=>250, 'min'=>6),
			array('cr_date', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, user_id, type_good, serv_id, part_id, game_id, count, cost, status, cr_date, userNick, typeGoodName,gameName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'serv' => array(self::BELONGS_TO, 'Serv', 'serv_id'),
			'type' => array(self::BELONGS_TO, 'TypeGoods', 'type_good'),
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'part' => array(self::BELONGS_TO, 'Part', 'part_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название товара',
			'user_id' => 'Владелец товара',
			'type_good' => 'Тип товара',
			'serv_id' => 'Сервер товара',
			'part_id' => 'Сторона товара',
			'game_id' => 'Игра товара',
			'count' => 'Количество единиц',
			'cost' => 'Стоимость за единицу',
			'status' => 'Статус',
			'cr_date' => 'Дата создания',
			'userNick' => 'Владелец товара',
			'typeGoodName' => 'Тип товара',
			'gameName' => 'Игра товара',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user','serv','type','game','part');

		$criteria->compare('id',$this->id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.user_id',$this->user_id);
		$criteria->compare('type_good',$this->type_good);
		$criteria->compare('serv_id',$this->serv_id);
		$criteria->compare('part_id',$this->part_id);
		$criteria->compare('t.game_id',$this->game_id);
		$criteria->compare('count',$this->count);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('status',$this->status);
		$criteria->compare('cr_date',$this->cr_date,true);

		$criteria->compare('user.nick',$this->userNick,true);
		$criteria->compare('type.name',$this->typeGoodName,true);
		$criteria->compare('game.name',$this->gameName,true);

		$sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes['userNick'] = array('asc' => 'user.nick','desc' => 'user.nick desc');
        $sort->attributes['typeGoodName'] = array('asc' => 'type.name','desc' => 'type.name desc');
        $sort->attributes['gameName'] = array('asc' => 'game.name','desc' => 'game.name desc');
        $sort->attributes['name'] = array('asc' => 't.name','desc' => 't.name desc');
        $sort->attributes['count'] = array('asc' => 't.count','desc' => 't.count desc');
        $sort->attributes['cost'] = array('asc' => 't.cost','desc' => 't.cost desc');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25,),
			'sort'=>$sort,
		));
	}

	public function beforeSave() {
        if (parent::beforeSave())
        {
        	if ($this->isNewRecord) {
        		$this->cr_date = time();
        		$this->status = 0;
        		$this->user_id = Yii::app()->user->id;
        	}
        	return true;
        } else {
        	return false;
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Goods the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	==============================================================
	============= Start: Поиск по связанным таблицам =============
	==============================================================
	*/

	/**
	 * Поле: user_id
	 */
	public function getUserNick() {
	    if ($this->_userNick === null && $this->userNick !== null) {
	        $this->_userNick = $this->user->nick;
	    }
	    return $this->_userNick;
	}
	public function setUserNick($value)
	{
	    $this->_userNick = $value;
	}

	/**
	 * Поле: type_good
	 */
	public function getTypeGoodName() {
	    if ($this->_typeGoodName === null && $this->typeGoodName !== null) {
	        $this->_typeGoodName = $this->type->name;
	    }
	    return $this->_typeGoodName;
	}
	public function setTypeGoodName($value)
	{
	    $this->_typeGoodName = $value;
	}

	/**
	 * Поле: game_id
	 */
	public function getGameName() {
	    if ($this->_gameName === null && $this->gameName !== null) {
	        $this->_gameName = $this->game->name;
	    }
	    return $this->_gameName;
	}
	public function setGameName($value)
	{
	    $this->_gameName = $value;
	}

	/*
	============================================================
	============= End: Поиск по связанным таблицам =============
	============================================================
	*/

}

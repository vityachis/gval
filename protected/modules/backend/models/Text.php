<?php

/**
 * This is the model class for table "{{text}}".
 *
 * The followings are the available columns in table '{{text}}':
 * @property integer $id
 * @property string $path
 * @property string $name
 * @property string $full_text
 * @property integer $user_id
 * @property string $cr_date
 * @property string $up_date
 */
class Text extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{text}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path, full_text, name', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('path, name', 'length', 'max'=>250),
			array('cr_date, up_date', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, path, name, full_text, user_id, cr_date, up_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'path' => 'Путь',
			'name' => 'Название',
			'full_text' => 'Полный текст',
			'user_id' => 'Создатель',
			'cr_date' => 'Дата создания',
			'up_date' => 'Дата редактирования',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user');

		$criteria->compare('id',$this->id);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('full_text',$this->full_text,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('cr_date',$this->cr_date,true);
		$criteria->compare('up_date',$this->up_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25,),
		));
	}

	public function beforeSave() {
		if (parent::beforeSave())
		{
			if ($this->isNewRecord) {
				$this->cr_date = time();

			}
			$this->user_id = Yii::app()->user->id;
			$this->up_date = time();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Text the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

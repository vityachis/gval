<?php

/**
 * This is the model class for table "{{game}}".
 *
 * The followings are the available columns in table '{{game}}':
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 */
class Game extends CActiveRecord
{

	private $_userNick;
	public $gameIcon;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{game}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gameIcon', 'file', 'types'=>'jpg', 'maxSize'=>'1024*1024', 'minSize'=>'1024*10', 'allowEmpty'=>true),
			array('name', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, user_id, userNick', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название игры',
			'user_id' => 'Добавил пользователь',
			'userNick' => 'Добавил пользователь',
			'gameIcon' => 'Картинка игры',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('user');

		$criteria->compare('id',$this->id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('user_id',$this->user->nick);

		$criteria->compare('user.nick',$this->userNick,true);

		$sort = new CSort();
        $sort->defaultOrder = 't.id DESC';
        $sort->attributes['userNick'] = array('asc' => 'user.nick','desc' => 'user.nick desc');
        $sort->attributes['name'] = array('asc' => 't.name','desc' => 't.name desc');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>25),
			'sort'=>$sort,
		));
	}

	public function beforeSave() {
        if (parent::beforeSave())
        {
        	if ($this->isNewRecord) {
        		$this->user_id = Yii::app()->user->id;
        	}
        	return true;
        } else {
        	return false;
        }
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Game the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	==============================================================
	============= Start: Поиск по связанным таблицам =============
	==============================================================
	*/

	/**
	 * Поле: user_id
	 */
	public function getUserNick() {
	    if ($this->_userNick === null && $this->userNick !== null) {
	        $this->_userNick = $this->user->nick;
	    }
	    return $this->_userNick;
	}
	public function setUserNick($value)
	{
	    $this->_userNick = $value;
	}

	/*
	============================================================
	============= End: Поиск по связанным таблицам =============
	============================================================
	*/
}

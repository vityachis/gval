<?php

class GoodsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '/layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','admin','delete','create','update','ajaxDropDownListServ','ajaxDropDownListPart'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Goods;

		$dropDownType = TypeGoods::model()->findAll();
		$dropDownGame = Game::model()->findAll();
		$dropDownServ = Serv::model()->findAll('game_id=1');
		$dropDownPart = Part::model()->findAll('game_id=1');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Goods']))
		{
			$model->attributes=$_POST['Goods'];
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Товар создан создан!');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'dropDownType'=>$dropDownType,
			'dropDownGame'=>$dropDownGame,
			'dropDownServ'=>$dropDownServ,
			'dropDownPart'=>$dropDownPart,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$dropDownType = TypeGoods::model()->findAll();
		$dropDownGame = Game::model()->findAll();
		$dropDownServ = Serv::model()->findAll($model->game_id);
		$dropDownPart = Part::model()->findAll($model->game_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Goods']))
		{
			$model->attributes=$_POST['Goods'];
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Изменения сохранены!');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'dropDownType'=>$dropDownType,
			'dropDownGame'=>$dropDownGame,
			'dropDownServ'=>$dropDownServ,
			'dropDownPart'=>$dropDownPart,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Goods('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Goods']))
			$model->attributes=$_GET['Goods'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Goods the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Goods::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function servListLoad() {

		$data=Serv::model()->findByPk('game_id=:game_id', array(':game_id'=>(int) $_POST['game_id']));
		foreach ($data as $dataOne) {
			$dropDownServSelect[$dataOne->id] = $dataOne->name;
		}
		echo $dropDownServSelect;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Goods $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='goods-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAjaxDropDownListServ($id) {
		$dropDownServ = Serv::model()->findAll('game_id=:game_id', array(':game_id'=>$id));
		if (count($dropDownServ) >= 1) {
			foreach ($dropDownServ as $dropDownServOne) {
				echo '<option value="' . $dropDownServOne->id . '">' . $dropDownServOne->name . '</option>';
			}
		} else {
			echo '<option value="0">Для выбраной игры серверов нет</option>';
		}
	}

	public function actionAjaxDropDownListPart($id) {
		$dropDownPart = Part::model()->findAll('game_id=:game_id', array(':game_id'=>$id));
		if (count($dropDownPart) >= 1) {
			foreach ($dropDownPart as $dropDownPartOne) {
				echo '<option value="' . $dropDownPartOne->id . '">' . $dropDownPartOne->name . '</option>';
			}
		} else {
			echo '<option value="0">Для выбраной игры сторон нет</option>';
		}
	}
}

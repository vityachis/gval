<?php

class DefaultController extends Controller
{

	public $layout = '/layouts/column1';

	public function filters()
	    {
	        return array(
	            'accessControl',
	        );
	    }

	public function accessRules()
	{
	    return array(
	        array('allow',  // позволим всем пользователям выполнять действия 'list' и 'show'
	            'actions'=>array('index'),
	            'users'=>array('admin'),
	        ),
	        array('deny',  // остальным запретим всё
	            'users'=>array('*'),
	        ),
	    );
	}

	public function actionIndex()
	{
		$countStatistic = array(
			'countGame' => '<b>' . Game::model()->count() . '</b>',
			'countServ' => '<b>' . Serv::model()->count() . '</b>',
			'countPart' => '<b>' . Part::model()->count() . '</b>',
			'countGoods' => '<b>' . Goods::model()->count('status<>2') . '</b>',
			'countOrder' => '<b>' . Order::model()->count('status<>0') . '</b>',
			'countUsers' => '<b>' . Users::model()->count('id<>777') . '</b>',
			'countUsersOnline' => '<b>' . Users::model()->count('online=1&&id<>777') . '</b>',
			'countUsersProd' => '<b>' . Users::model()->count('rule>=3') . '</b>',
			'countUsersPocup' => '<b>' . Users::model()->count('rule>=2') . '</b>',
			'countUsersAdmin' => '<b>' . Users::model()->count('rule>=4') . '</b>',
			'countMail' => '<b>' . Mail::model()->count('to_id<>777') . '</b>',
			'countText' => '<b>' . Text::model()->count() . '</b>',
			'countRevie' => '<b>' . Revie::model()->count() . '</b>',
			'customServer' => '<b>' . TgameServer::model()->count('status=1') . '</b>',
		);

		$this->render('index',
			array(
				'layout' => $this->layout,
				'countStatistic' => $countStatistic,
			));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}

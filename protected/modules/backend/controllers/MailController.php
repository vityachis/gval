<?php

class MailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '/layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','delete','update','mail_list','corr'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Mail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mail']))
		{
			$model->attributes=$_POST['Mail'];
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS,'Сообщение успешно создано.');
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mail']))
		{
			$model->attributes=$_POST['Mail'];
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Изменения сохранены!');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$count = Users::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = 25;
		$pages->applyLimit($criteria);

		$model = Users::model()->with('mailNickUsersTo', 'mailNickUsersFrom')->findAll($criteria);
		$modelMail=new Mail('search');
		$modelMail->unsetAttributes();  // clear any default values
		if(isset($_GET['Mail']))
			$modelMail->attributes=$_GET['Mail'];
		$this->render('index',array(
		    'model'=>$model,
		    'pages'=>$pages,
		    'modelMail'=>$modelMail,
		));
	}

	public function actionMail_list($id) {
	    $mailListTo = Mail::model()->with('userDataTo')->findAll('to_id=:to_id', array(':to_id' => $id));
	    $mailListFrom = Mail::model()->with('userDataFrom')->findAll('from_id=:from_id', array(':from_id' => $id));
	    $model = Users::model()->findByPk($id);

	    $this->render('mail_list', array(
	        'model' => $model,
	        'mailListTo' => $mailListTo,
	        'mailListFrom' => $mailListFrom,
	    ));
	}

	public function actionCorr($id, $corr) {
	    $mailCorr = Mail::model()->with('userData')->findAll('(from_id=:fromIdOne AND to_id=:toIdOne) OR (from_id=:fromIdTwo AND to_id=:toIdTwo)',
	        array('fromIdOne'=>$id, 'toIdOne'=>$corr, 'fromIdTwo'=>$corr, 'toIdTwo'=>$id)
	    );
	    $mailCorrUserTo = Users::model()->findByPk($id);
	    $mailCorrUserFrom = Users::model()->findByPk($corr);
	    $this->render('corr', array(
	        'mailCorr' => $mailCorr,
	        'mailCorrTitl' => $mailCorrTitl,
	        'mailCorrUserTo' => $mailCorrUserTo,
	        'mailCorrUserFrom' => $mailCorrUserFrom,
	    ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

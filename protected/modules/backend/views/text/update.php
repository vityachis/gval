<?php
/* @var $this TextController */
/* @var $model Text */

$this->pageTitle='Панель управления - Редактировать статический текст ('.$model->name.')';
?>

<h1>Редактировать статический текст: <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
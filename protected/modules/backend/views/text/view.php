<?php
/* @var $this TextController */
/* @var $model Text */

$this->pageTitle='Панель управления - Просмотр статического текста ('.$model->name.')';
?>

<h1>Просмотр статического текста: <?php echo $model->name; ?>
	<a href="<?=$this->createUrl('/backend/text/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'path',
		'name',
		array('name'=>'user_id', 'value'=>$model->user->nick),
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
		array('name'=>'up_date', 'value'=>date('d.m.Y H:i:s', $model->up_date)),
	),
)); ?>

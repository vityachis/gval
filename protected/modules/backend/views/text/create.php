<?php
/* @var $this TextController */
/* @var $model Text */

$this->pageTitle='Панель управления - Создать статический текст';
?>

<h1>Создать статический текст</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
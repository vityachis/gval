<?php
/* @var $this TextController */
/* @var $data Text */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('path')); ?>:</b>
	<?php echo CHtml::encode($data->path); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_text')); ?>:</b>
	<?php echo CHtml::encode($data->full_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cr_date')); ?>:</b>
	<?php echo CHtml::encode($data->cr_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('up_date')); ?>:</b>
	<?php echo CHtml::encode($data->up_date); ?>
	<br />


</div>
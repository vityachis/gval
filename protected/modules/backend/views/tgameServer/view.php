<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
$this->pageTitle='Панель управления - Просмотр сервера: '.$model->name.'['.$model->id.']';
if ($model->status == 0) $textStatus = '<span style="color:yellow;font-weight:bold;">Ждет модерации</span>';
else if ($model->status == 1) $textStatus = '<span style="color:green;font-weight:bold;">Опубликованно</span>';
else $textStatus = '<span style="color:red;font-weight:bold;">Публикация отменена</span>';
?>

<h1>Просмотр сервера: <?=$model->name?> [<?=$model->id?>]
	<a href="<?=$this->createUrl('/backend/tgameserver/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		'name',
		'ret',
		array('name'=>'game_id', 'value'=>$model->game->name),
		array('name'=>'status','type'=>'html' ,'value'=>$textStatus),
		array('name'=>'href','type'=>'html' ,'value'=>'<a href="'.$model->href.'">'.$model->href.'</a>'),
		array('name'=>'user_id', 'value'=>$model->cr_user->nick),
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
		array('name'=>'moder_id', 'value'=>$model->pb_user->nick),
	),
)); ?>

<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
$this->pageTitle='Панель управления - Создать сервер';
?>

<h1>Создать пользовательский сервер</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelGame'=>$modelGame)); ?>

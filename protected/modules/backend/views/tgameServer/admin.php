<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
$this->pageTitle='Панель управления - Менеджер серверов';
?>

<h1>Менеджер пользовательских серверов</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'tgame-server-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		array(
			'name'=>'name',
			'type'=>'html',
			'value'=>'CHtml::link($data->name, array(\'/backend/tgameserver/view\', \'id\'=>$data->id))'),
		'ret',
		array(
			'name'=>'gameName',
			'type'=>'html',
			'value'=>'CHtml::link($data->game->name, array(\'/backend/game/view\', \'id\'=>$data->game->id))'),
		// 'status',
		'href',
		/*
		'user_id',
		'cr_date',
		'moder_id',
		*/
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this TgameServerController */
/* @var $model TgameServer */
$this->pageTitle='Панель управления - Редактировать сервер: '.$model->name.'['.$model->id.']';
?>

<h1>Редактировать сервер: <?=$model->name?> [<?=$model->id?>]</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'modelGame'=>$modelGame)); ?>

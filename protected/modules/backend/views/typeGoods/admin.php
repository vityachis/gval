<?php
/* @var $this TypeGoodsController */
/* @var $model TypeGoods */

$this->pageTitle='Панель управления - Менеджер типов товаров';
?>

<h1>Менеджер типов товаров</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'type-goods-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		array('name'=>'name','type'=>'html','value'=>'CHtml::link($data->name, array(\'/backend/typeGoods/view\', \'id\'=>$data->id))'),
		// 'user_id',
		// 'cr_date',
		// 'up_date',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

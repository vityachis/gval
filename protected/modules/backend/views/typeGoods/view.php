<?php
/* @var $this TypeGoodsController */
/* @var $model TypeGoods */

$this->pageTitle='Панель управления - Просмотр типа товара ('.$model->name.')';
?>

<h1>Просмотр типа товара: <?php echo $model->name; ?> 
	<a href="<?=$this->createUrl('/backend/typeGoods/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('name'=>'user_id','value'=>$model->user->nick),
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
		array('name'=>'up_date', 'value'=>date('d.m.Y H:i:s', $model->up_date)),
	),
)); ?>

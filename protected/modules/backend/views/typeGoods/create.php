<?php
/* @var $this TypeGoodsController */
/* @var $model TypeGoods */

$this->pageTitle='Панель управления - Создать тип товара';
?>

<h1>Создать тип товара</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
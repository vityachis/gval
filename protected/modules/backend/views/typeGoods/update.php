<?php
/* @var $this TypeGoodsController */
/* @var $model TypeGoods */

$this->pageTitle='Панель управления - Редактировать тип товара ('.$model->name.')';
?>

<h1>Редактировать тип товара: <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
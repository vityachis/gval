<?php
/* @var $this GameController */
/* @var $model Game */

$this->pageTitle='Панель управления - Создать игру';
?>

<h1>Создать игру</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this GameController */
/* @var $model Game */

$this->pageTitle='Панель управления - Менеджер игр';
?>

<h1>Менеджер игр</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'game-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'name',
			'type'=>'html',
			'value'=>'CHtml::link($data->name, array(\'/backend/game/view\', \'id\'=>$data->id))'),
		array(
			'name'=>'userNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->user->nick, array(\'/backend/users/view\', \'id\'=>$data->user->id))'),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

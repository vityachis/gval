<?php
/* @var $this GameController */
/* @var $model Game */

$this->pageTitle='Панель управления - Просмотр игры ('.$model->name.')';
?>

<h1>Просмотр игры: <?php echo $model->name; ?>
	<a href="<?=$this->createUrl('/backend/game/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('name'=>'user_id','value'=>$model->user->nick),
	),
)); ?>

<?php
/* @var $this GameController */
/* @var $model Game */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'id'=>'game-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'class'=>'form-narrow',
		'enctype'=>'multipart/form-data',
		),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> заполнять обязательно.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'gameIcon'); ?>
		<?php echo $form->fileField($model,'gameIcon'); ?>
		<span class="help-block">Желательно загружать файлы: 270x175</span>
		<?php echo $form->error($model,'gameIcon'); ?>
	</div>

	<?if (!$model->isNewRecord):?>
		<div class="form-group">
			<label for="gameIcon">Отображаемая картинка</label>
			<div class="logo" style="width:268px;height:175px;background:url(/images/game_icon/<?=WorkView::imageGame($model->id)?>.jpg) no-repeat 0 0;"></div>
		</div>
	<?endif?>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
/* @var $this GameController */
/* @var $model Game */

$this->pageTitle='Панель управления - Редактировать игру ('.$model->name.')';
?>

<h1>Редактировать игру: <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
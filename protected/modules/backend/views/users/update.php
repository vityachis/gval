<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle='Панель управления - Редактировать пользователя ('.$model->name.')';
?>
<h1>Редактировать пользователя: <?php echo $model->name; ?> (ID: <?=$model->id?>)</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'DropDownRole'=>$DropDownRole)); ?>

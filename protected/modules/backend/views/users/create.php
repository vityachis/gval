<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle='Панель управления - Создать пользователя';
?>

<h1>Создать пользователя</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'DropDownRole'=>$DropDownRole)); ?>

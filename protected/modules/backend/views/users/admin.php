<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle='Панель управления - Менеджер пользователей';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Менеджер пользователей</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		// 'vk_id',
		array('name'=>'nick','type'=>'html','value'=>'CHtml::link($data->nick, array(\'/backend/users/view\', \'id\'=>$data->id))'),
		'name',
		// 'surname',
		'email',
		// 'pass',
		// 'pol',
		// 'rule',
		// 'ban',
		// 'ban_time',
		// 'lvl_ban',
		// 'icq',
		// 'skype',
		// 'date_reg',
		// 'date_last',
		// 'patron',
		// 'online',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

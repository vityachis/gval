<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions' => array('class'=>'form-narrow'),
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'pol'); ?>
		<?php echo $form->dropDownList($model, 'pol', array('1'=>'Мужской', '2'=>'Женский'), array('empty'=>'Не выбрано')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'rule'); ?>
		<?php echo $form->dropDownList($model,'rule',TbHtml::listData(Role::model()->findAll(), 'id', 'name'), array('empty'=>'Не выбрано')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'ban'); ?>
		<?php echo $form->dropDownList($model, 'ban', array('1'=>'Забанен', '0'=>'Бан отсутствует'), array('empty'=>'Не выбрано')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'icq'); ?>
		<?php echo $form->textField($model,'icq'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'skype'); ?>
		<?php echo $form->textField($model,'skype',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'patron'); ?>
		<?php echo $form->textField($model,'patron',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="form-group buttons">
		<?php echo TbHtml::submitButton('Искать'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

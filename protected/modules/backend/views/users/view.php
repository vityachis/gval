<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle='Панель управления - Просмотр пользователя ('.$model->nick.')';
?>

<h1>Просмотр пользователя: <?php echo $model->nick; ?> (ID: <?=$model->id?>)
	<a href="<?=$this->createUrl('/backend/users/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
	<?if ($model->role->id !== 5):?>
		<!--<a href="<?=$this->createUrl('/backend/users/adminAccess', array('id'=>$model->id))?>">-->

			<?php $this->widget('bootstrap.widgets.TbModal', array(
			    'id' => 'myModal',
			    'header' => 'Вход под пользователем ' . $model->nick,
			    'content' => '<h5>Если Вы войдете под другим пользователем, Ваши права доступа будут потеряны! Для последующей работы с Панелью управление, нужно будет пройти авторизацию заново!</h5>',
			    'footer' => array(
			        TbHtml::button('Да, войти под пользователем', array('data-id'=>$model->id, 'id'=>'adminAccess', 'data-dismiss' => 'modal', 'color' => TbHtml::BUTTON_COLOR_PRIMARY)),
			        TbHtml::button('Отмена', array('data-dismiss' => 'modal')),
			     ),
			)); ?>

			<?php echo TbHtml::button('Войти под пользователем', array(
			    'color' => TbHtml::BUTTON_COLOR_INFO,
			    'data-toggle' => 'modal',
			    'data-target' => '#myModal',
			)); ?>

		<!--</a>-->
	<?endif?>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if ($model->ban !== '0') {
	$banValue = 'Забанен до ' . date('Y.m.d H:i:s', $model->ban_time + $model->lvl_ban);
	} else {$banValue = 'Отсутствует';}?>
<?if ($model->online !== '0') {$onValue = 'Сейчас в сети';} else {$onValue = date('d.m.Y H:i:s', $model->date_last);}?>
<?if ($model->pol == '1') {$polValue = 'Мужской';} else if ($model->pol == '2') {$polValue = 'Женский';} else {$polValue = 'Ошибка!';}?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		'nick',
		'name',
		'surname',
		'patron',
		array('name'=>'pol', 'value'=>$polValue),
		'email',
		// 'pass',
		'vk_id',
		array('name'=>'rule', 'value'=>$model->role->name),
		array('name'=>'ban', 'value'=>$banValue),
		'icq',
		'skype',
		array('name'=>'date_reg', 'value'=>date('d.m.Y H:i:s', $model->date_reg)),
		array('name'=>'date_last', 'value'=>$onValue),
	),
)); ?>

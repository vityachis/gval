<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="ru">
	<?php Yii::app()->bootstrap->register(); ?>

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/css/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/myStyle.css">
	<style>
		nav.navbar > div:nth-child(1) {
			width:100%;
		}
		footer.main-footer > div:nth-child(1) {
			width:100%;
		}
		div.container:nth-child(2) {
			width:100%;
		}
	</style>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
</head>

<body>

<div class="container" id="page">
	<div id="header">
		<?php $this->widget('bootstrap.widgets.TbNavbar', array(
		    'brandLabel' => 'Панель управления',
		    'brandUrl'=>'/backend',
		    'color' => TbHtml::NAVBAR_COLOR_INVERSE,
		    'htmlOptions' => array('style'=>'border-radius:0px;'),
		    'collapse' => true,
		    'display' => null, // default is static to top
		    'items' => array(
		        array(
		            'class' => 'bootstrap.widgets.TbNav',
		            'items' => array(
		            	array('label'=>'Пользователи', 'items'=>array(
		            		array('label'=>'Создать пользователя', 'url'=>'/backend/users/create'),
		            		array('label'=>'Менеджер пользователей', 'url'=>'/backend/users/admin'),
		            	)),
			            array('label'=>'Товары', 'items'=>array(
			            	array('label'=>'Создать товар', 'url'=>'/backend/goods/create'),
			            	array('label'=>'Менеджер товаров', 'url'=>'/backend/goods/admin'),
			            )),
			            array('label'=>'Менеджер заказов', 'url'=>'/backend/order/admin'),
			            array('label'=>'Пользовательские cервера', 'items'=>array(
			            	array('label'=>'Создать сервер', 'url'=>'/backend/tgameserver/create'),
			            	array('label'=>'Менеджер серверов', 'url'=>'/backend/tgameserver/admin'),
			            )),
			            array('label'=>'Статический текст', 'items'=>array(
			            	array('label'=>'Создать текст', 'url'=>'/backend/text/create'),
			            	array('label'=>'Менеджер текстов', 'url'=>'/backend/text/admin'),
			            )),
		            ),
		        ),
		        array(
		            'class' => 'bootstrap.widgets.TbNav',
		            'encodeLabel'=>false,
		            'htmlOptions' => array('class'=>'navbar-right'),
		            'items' => 	array(
		            	array('label' => Yii::app()->user->name, 'url' => '/users/cabinet'),
		            	array('label' => "<span class='glyphicon glyphicon-eye-open'></span>",
		            		'url' => '/site/index',
		            		'id'=>'adm-site'),
						array('label' => "<span class='glyphicon glyphicon-log-out'></span>", 'url' => '/site/logout'),
					),
		        ),
		    ),
		)); ?>
	</div><!-- header -->
	<div style='width: 18%;float:left;margin-right:2%;'>
		<?$this->widget('bootstrap.widgets.TbNav', array(
		    'type' => TbHtml::NAV_TYPE_LIST,
		    'encodeLabel'=>false,
		    'items' => array(
		        array('label' => '<b>Управление играми:</b>'),
		        array('label' => 'Создать игру', 'url' => '/backend/game/create'),
		        array('label' => 'Менеджер игр', 'url' => '/backend/game/admin'),
		        array('label' => '<b>Управление серверами:</b>'),
		        array('label' => 'Создать сервер', 'url' => '/backend/serv/create'),
		        array('label' => 'Менеджер серверов', 'url' => '/backend/serv/admin'),
		        array('label' => '<b>Управление сторонами:</b>'),
		        array('label' => 'Создать сторону', 'url' => '/backend/part/create'),
		        array('label' => 'Менеджер сторон', 'url' => '/backend/part/admin'),
		        array('label' => '<b>Управление отзывами:</b>'),
		        array('label' => 'Менеджер отзывов', 'url' => '/backend/revie/admin'),
		        array('label' => '<b>Управление типом товаров</b>'),
		        array('label' => 'Создать тип товара', 'url' => '/backend/typeGoods/create'),
		        array('label' => 'Менеджер типов товаров', 'url' => '/backend/typeGoods/admin'),
		        array('label' => '<b>Управление ценами</b>'),
		        array('label' => 'Создать цену', 'url' => '/backend/prices/create'),
		        array('label' => 'Менеджер цен', 'url' => '/backend/prices/admin'),
		    )
		));?>
	</div>
		<?php echo $content; ?>
	<footer class="main-footer" style='background-color:#222;border-color:#080808;'>
	  <div class="container">
	    <ul class="left">
	      <li>© <?=date('Y')?> GameVal</li>
	      <li>Обратная связь: <a href="mailto:vityachis@ya.ru">vityachis@ya.ru</a></li>
	    </ul>
	  </div>
	</footer>

</div><!-- page -->

</body>
</html>

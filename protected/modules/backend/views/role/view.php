<?php
/* @var $this RoleController */
/* @var $model Role */

$this->pageTitle='Панель управления - Просмотр роли ('.$model->name.')';
?>

<h1>Просмотр роли: <?php echo $model->name; ?>
	<a href="<?=$this->createUrl('/backend/role/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>

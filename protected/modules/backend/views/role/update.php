<?php
/* @var $this RoleController */
/* @var $model Role */

$this->pageTitle='Панель управления - Редактировать роль ('.$model->name.')';
?>

<h1>Редактировать роль: <?php echo $model->name; ?> (уровень: <?=$model->id?>)</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
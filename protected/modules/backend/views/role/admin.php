<?php
/* @var $this RoleController */
/* @var $model Role */

$this->pageTitle='Панель управления - Менеджер ролей';
?>

<h1>Менеджер ролей</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'role-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array('name'=>'name','type'=>'html','value'=>'CHtml::link($data->name, array(\'/backend/role/view\', \'id\'=>$data->id))'),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

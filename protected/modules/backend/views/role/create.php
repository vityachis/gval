<?php
/* @var $this RoleController */
/* @var $model Role */

$this->pageTitle='Панель управления - Создать роль';
?>

<h1>Создать роль</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Ошибка '.$code;
?>

<?$errorText = "<h2>Ошибка $code:</h2><br />" . CHtml::encode($message)?>

<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_DANGER, $errorText); ?>
<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
$this->pageTitle='Панель управления';
?>

<?php
$statistic = '<b style="font-size: 17px;">Статистика по категориям:</b> <br />';
$statistic .= 'Количество игр в системе: ' . $countStatistic['countGame'] . '<br />';
$statistic .= 'Количество северов в системе: ' . $countStatistic['countServ'] . '<br />';
$statistic .= 'Количество сторон в системе: ' . $countStatistic['countPart'] . '<br />';
$statistic .= '<b style="font-size: 17px;">Статистика по товарам:</b> <br />';
$statistic .= 'Количество товаров в системе: ' . $countStatistic['countGoods'] . '<br />';
$statistic .= 'Количество заказов в системе: ' . $countStatistic['countOrder'] . '<br />';
$statistic .= '<b style="font-size: 17px;">Статистика по пользователям:</b> <br />';
$statistic .= 'Количество пользователей в системе: ' . $countStatistic['countUsers'] . '<br />';
$statistic .= 'Количество пользователей онлайн в системе: ' . $countStatistic['countUsersOnline'] . '<br />';
$statistic .= 'Количество продавцов в системе: ' . $countStatistic['countUsersProd'] . '<br />';
$statistic .= 'Количество покупателей в системе: ' . $countStatistic['countUsersPocup'] . '<br />';
$statistic .= 'Количество административного состава в системе: ' . $countStatistic['countUsersAdmin'] . '<br />';
$statistic .= '<b style="font-size: 17px;">Прочая статистика:</b> <br />';
$statistic .= 'Общее количество сообщений в системе: ' . $countStatistic['countMail'] . '<br />';
$statistic .= 'Общее количество статических текстов в системе: ' . $countStatistic['countText'] . '<br />';
$statistic .= 'Общее количество отзывов в системе: ' . $countStatistic['countRevie'] . '<br />';
$statistic .= 'Общее количество пользовательских серверов: ' . $countStatistic['customServer'] . '<br />';
?>

<?php echo TbHtml::heroUnit(Yii::app()->name, $statistic); ?>

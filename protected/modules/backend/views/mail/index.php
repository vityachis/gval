<?php
/* @var $this MailController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle='Панель управления - Список диалогов';
?>

<h1>Список диалогов</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?/*php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'mail-grid',
	'dataProvider'=>$modelMail->search(),
	'filter'=>$modelMail,
	'columns'=>array(
		array('name'=>'status','type'=>'html','value'=>'CHtml::link($data->id, array(\'/backend/mail/mail_list\', \'id\'=>$data->id))'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); */?>

<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Диалог</th>
			<th>Входящих</th>
			<th>Исходящих</th>
		</tr>
	</thead>
	<tbody>
	<?foreach ($model as $mailOne):?>
		<tr>
			<th><a href="<?=$this->createUrl('/backend/mail/mail_list', array('id'=>$mailOne->id))?>"><?=$mailOne->nick?></a></th>
			<td><?=count($mailOne->mailNickUsersFrom)?></td>
			<td><?=count($mailOne->mailNickUsersTo)?></td>
		</tr>
	<?endforeach?>
	</tbody>
</table>

<?php
/* @var $this MailController */

$this->pageTitle='Панель управления - Список диалогов (' . $mailCorrUserTo->nick . ')';
?>

<h1>Переписка: <?=$mailCorrUserTo->nick?> и <?=$mailCorrUserFrom->nick?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>Дата</th>
				<th>Ник</th>
				<th>Сообщение</th>
				<th>Действия</th>
			</tr>
		</thead>
		<tbody>
		<?foreach ($mailCorr as $mailCorrList):?>
			<tr>
				<td><?=date('d.m.Y H:i:s', $mailCorrList->send_date)?></td>
				<td><?=$mailCorrList->userData->nick?></td>
				<td><?=$mailCorrList->text_mail?></td>
				<td>
					<?=CHtml::link('<img style="float: right;" src="/assets/3ce7a7a9/gridview/delete.png">', array('delete', 'id'=>$mailCorrList->id));?>
					<?=CHtml::link('<img style="float: right;" src="/assets/3ce7a7a9/gridview/update.png">', array('update', 'id'=>$mailCorrList->id));?>
				</td>
			</tr>
		<?endforeach?>
		</tbody>
	</table>
	<?$this->widget('CLinkPager', array(
		'header' => '',
	    'pages' => $pages,
	    'htmlOptions'=> array('class'=>'pagination'),
	));?>
</div>

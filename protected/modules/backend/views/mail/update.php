<?php
/* @var $this MailController */
/* @var $model Mail */

$this->pageTitle='Панель управления - Редактировать сообщение';
?>

<h1>Редактировать сообщение <?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
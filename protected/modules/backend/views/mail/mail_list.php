<?php
/* @var $this MailController */
/* @var $model Mail */
?>

<h1>Список переписок (<?=$model->nick?>)</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?
if (count($mailListTo) >= 1) {
	foreach ($mailListTo as $mailListToId) {
		$mailListId[$mailListToId->userDataTo->id] = $mailListToId->userDataTo->nick;
	}
} else if (count($mailListFrom) >= 1) {
	foreach ($mailListFrom as $mailListFromId) {
		$mailListId[$mailListFromId->userDataFrom->id] = $mailListFromId->userDataFrom->nick;
	}
} else {
	$mailListId = null;
}
?>
<pre>
<?print_r($mailListFrom)?>
</pre>
<div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>Переписка с</th>
			</tr>
		</thead>
		<tbody>
		<?if ($mailListId !== null):?>
			<?foreach($mailListId as $mailListLinkId=>$mailListLinkName):?>
				<tr>
					<th><?=CHtml::link(CHtml::encode($mailListLinkName), array('corr', 'id'=>$model->id, 'corr'=>$mailListLinkId))?></th>
				</tr>
			<?endforeach?>
			<?else:?>
				<tr>
					<th>Переписки не найдены</th>
				</tr>
			<?endif?>
		</tbody>
	</table>
	<?$this->widget('CLinkPager', array(
		'header' => '',
	    'pages' => $pages,
	    'htmlOptions'=> array('class'=>'pagination'),
	));?>
</div>

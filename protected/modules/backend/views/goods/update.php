<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle='Панель управления - Редактировать товар ('.$model->name.')';
?>

<h1>Редактировать товар: <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownType'=>$dropDownType,
    'dropDownGame'=>$dropDownGame,
    'dropDownServ'=>$dropDownServ,
    'dropDownPart'=>$dropDownPart,
)); ?>

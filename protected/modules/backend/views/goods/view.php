<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle='Панель управления - Просмотр товара ('.$model->name.')';
?>

<h1>Просмотр товара: <?php echo $model->name; ?>
	<a href="<?=$this->createUrl('/backend/goods/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?if ($model->status == 0) {
	$statusValue = array('name'=>'status','type'=>'html','value'=>'<span style="color:green;font-weight:bold;">Продается</span>');
} else if ($model->status == 1) {
	$statusValue = array('name'=>'status','type'=>'html','value'=>'<span style="color:#fad201;font-weight:bold;">Заказан</span>');
} else if ($model->status == 2) {
	$statusValue = array('name'=>'status','type'=>'html','value'=>'<span style="color:red;font-weight:bold;">Выкуплен</span>');
}
?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('name'=>'user_id','value'=>$model->user->nick),
		array('name'=>'type_good','value'=>$model->type->name),
		array('name'=>'game_id','value'=>$model->game->name),
		array('name'=>'serv_id','value'=>$model->serv->name),
		array('name'=>'part_id','value'=>$model->part->name),
		'count',
		'cost',
		$statusValue,
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
	),
)); ?>

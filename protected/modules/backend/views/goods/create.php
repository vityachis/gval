<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle='Панель управления - Создать товар';
?>

<h1>Создать товар</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownType'=>$dropDownType,
    'dropDownGame'=>$dropDownGame,
    'dropDownServ'=>$dropDownServ,
    'dropDownPart'=>$dropDownPart,
)); ?>

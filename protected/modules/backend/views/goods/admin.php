<?php
/* @var $this GoodsController */
/* @var $model Goods */

$this->pageTitle='Панель управления - Менеджер товаров';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#goods-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Менеджер товаров
	<a href="<?=$this->createUrl('/goods/cabinet')?>"><button type="button" class="btn btn-info">Мои товары</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'goods-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		array('name'=>'name',
			'type'=>'html',
			'value'=>'CHtml::link($data->name, array(\'/backend/goods/view\', \'id\'=>$data->id))',
		),
		array(
			'name'=>'userNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->user->nick, array(\'/backend/users/view\', \'id\'=>$data->user->id))',
		),
		array(
			'name'=>'typeGoodName',
			'type'=>'html',
			'value'=>'CHtml::link($data->type->name, array(\'/backend/typeGoods/view\', \'id\'=>$data->type->id))',
		),
		array(
			'name'=>'gameName',
			'type'=>'html',
			'value'=>'CHtml::link($data->game->name, array(\'/backend/game/view\', \'id\'=>$data->game->id))',
		),
		'count',
		'cost',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this PartController */
/* @var $model Part */

$this->pageTitle='Панель управления - Создать сторону';
?>

<h1>Создать сторону</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownGame'=>$dropDownGame,
)); ?>

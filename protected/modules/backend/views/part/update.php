<?php
/* @var $this PartController */
/* @var $model Part */

$this->pageTitle='Панель управления - Редактировать сторону ('.$model->name.')';
?>

<h1>Редактировать сторону <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'dropDownGame'=>$dropDownGame,
)); ?>

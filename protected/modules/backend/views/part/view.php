<?php
/* @var $this PartController */
/* @var $model Part */

$this->pageTitle='Панель управления - Просмотр стороны ('.$model->name.')';
?>

<h1>Просмотр стороны: <?php echo $model->name; ?>
	<a href="<?=$this->createUrl('/backend/part/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('name'=>'game_id','value'=>$model->game->name),
		array('name'=>'user_id','value'=>$model->user->nick),
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
	),
)); ?>

<?php
/* @var $this PartController */
/* @var $model Part */

$this->pageTitle='Панель управления - Менеджер сторон';
?>

<h1>Менеджер сторон</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'part-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'name',
			'type'=>'html',
			'value'=>'CHtml::link($data->name, array(\'/backend/part/view\', \'id\'=>$data->id))',
		),
		array(
			'name'=>'gameName',
			'type'=>'html',
			'value'=>'CHtml::link($data->game->name, array(\'/backend/game/view\', \'id\'=>$data->game->id))',
		),
		array(
			'name'=>'userNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->user->nick, array(\'/backend/users/view\', \'id\'=>$data->user->id))',
		),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

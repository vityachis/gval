<?php
/* @var $this PricesController */
/* @var $model Prices */
$this->pageTitle='Панель управления - Просмотр цены';
?>

<h1>Просмотр цены
	<a href="<?=$this->createUrl('/backend/prices/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'valuePrice',
		'keyPrice',
		'price',
	),
)); ?>

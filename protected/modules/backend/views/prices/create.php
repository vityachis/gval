<?php
/* @var $this PricesController */
/* @var $model Prices */
$this->pageTitle='Панель управления - Создать цену';

?>

<h1>Создать цену</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php
/* @var $this PricesController */
/* @var $model Prices */
$this->pageTitle='Панель управления - Менеджер цен';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prices-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Менеджер цен</h1>

<p>
Вы можете дополнительно ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого из значений поиска, чтобы указать, как должно быть сделано сравнение.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'prices-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'valuePrice','type'=>'html','value'=>'CHtml::link($data->valuePrice, array(\'/backend/prices/view\', \'id\'=>$data->id))'),
		'keyPrice',
		'price',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this PricesController */
/* @var $model Prices */
$this->pageTitle='Панель управления - Редактировать цену';
?>

<h1>Редактировать цену</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php
/* @var $this OrderController */
/* @var $model Order */

$this->pageTitle='Панель управления - Редактировать заказ ('.$model->id.')';
?>

<h1>Редактировать заказ: <?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
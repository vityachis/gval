<?php
/* @var $this OrderController */
/* @var $model Order */

$this->pageTitle='Панель управления - Менеджер заказов';
?>

<h1>Менеджер заказов
	<a href="<?=$this->createUrl('/order/cabinet')?>"><button type="button" class="btn btn-info">Мои заказы</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'order-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'goodName',
			'type'=>'html',
			'value'=>'CHtml::link($data->good->name, array(\'/backend/order/view\', \'id\'=>$data->id))',
		),
		array(
			'name'=>'userNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->user->nick, array(\'/backend/users/view\', \'id\'=>$data->user->id))',
		),
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

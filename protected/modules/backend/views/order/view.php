<?php
/* @var $this OrderController */
/* @var $model Order */

$this->pageTitle='Панель управления - Просмотр заказа ('.$model->id.')';
?>

<h1>Просмотр заказа: <?php echo $model->id; ?>
	<a href="<?=$this->createUrl('/backend/order/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php
if ($model->status == '1') {
	$statusValue = "<span style='color:green;font-weight:bold;'>Товар заказан</span>";
} else if ($model->status == '0') {
	$statusValue = "<span style='color:red;font-weight:bold;'>Товар куплен</span>";
}
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('name'=>'id_good','type'=>'html','value'=>CHtml::link($model->goods->name, array('/backend/goods/view', 'id'=>$model->id_good))),
		array('name'=>'id_user','value'=>$model->user->nick),
		array('name'=>'status','type'=>'html','value'=>$statusValue),
	),
)); ?>

<?php
/* @var $this RevieController */
/* @var $model Revie */

$this->pageTitle='Панель управления - Редактировать отзыв ('.$model->user_id.' о '.$model->from_id.')';
?>

<h1>Редактировать отзыв: <?=$model->user_id?> о <?=$model->from_id?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

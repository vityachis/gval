<?php
/* @var $this RevieController */
/* @var $data Revie */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from_user')); ?>:</b>
	<?php echo CHtml::encode($data->from_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ret')); ?>:</b>
	<?php echo CHtml::encode($data->ret); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cr_date')); ?>:</b>
	<?php echo CHtml::encode($data->cr_date); ?>
	<br />


</div>
<?php
/* @var $this RevieController */
/* @var $model Revie */

$this->pageTitle='Панель управления - Просмотр отзыва ('.$model->to->nick.' - '.$model->from->nick.')';
?>

<h1>Просмотр отзыва: <?=$model->to->nick?> - <?=$model->from->nick?> 
	<a href="<?=$this->createUrl('/backend/revie/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('name'=>'from_id','value'=>$model->from->nick),
		array('name'=>'user_id','value'=>$model->to->nick),
		'text',
		'ret',
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
	),
)); ?>

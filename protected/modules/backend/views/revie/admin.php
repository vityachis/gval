<?php
/* @var $this RevieController */
/* @var $model Revie */

$this->pageTitle='Панель управления - Менеджер отзывов';
?>

<h1>Менеджер отзывов</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<p>
Дополнительно, вы можете ввести оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого вашего поиска значений, чтобы указать, как сравнение должно быть сделано.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'revie-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'filter'=>'',
			'name'=>'id',
			'type'=>'html',
			'value'=>'CHtml::link("Просмотреть отзыв [$data->id]", array(\'/backend/revie/view\', \'id\'=>$data->id))',
		),
		array(
			'name'=>'toNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->to->nick, array(\'/backend/users/view\', \'id\'=>$data->to->id))',
		),
		array(
			'name'=>'fromNick',
			'type'=>'html',
			'value'=>'CHtml::link($data->from->nick, array(\'/backend/users/view\', \'id\'=>$data->from->id))',
		),
		'ret',
		array(
			'class'=>'TbButtonColumn',
		),
	),
)); ?>

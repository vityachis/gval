<?php
/* @var $this ServController */
/* @var $model Serv */

$this->pageTitle='Панель управления - Создать сервер';
?>

<h1>Создать сервер</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'selectGame'=>$selectGame)); ?>

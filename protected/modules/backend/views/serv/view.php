<?php
/* @var $this ServController */
/* @var $model Serv */

$this->pageTitle='Панель управления - Просмотр сервера ('.$model->name.')';
?>

<h1>Просмотр сервера: <?php echo $model->name; ?> 
	<a href="<?=$this->createUrl('/backend/serv/update', array('id'=>$model->id))?>"><button type="button" class="btn btn-info">Редактировать</button></a>
</h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('name'=>'game_id','value'=>$model->game->name),
		'name',
		array('name'=>'user_id','value'=>$model->user->nick),
		array('name'=>'cr_date', 'value'=>date('d.m.Y H:i:s', $model->cr_date)),
		array('name'=>'up_date', 'value'=>date('d.m.Y H:i:s', $model->up_date)),
	),
)); ?>

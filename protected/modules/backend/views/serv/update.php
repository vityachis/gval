<?php
/* @var $this ServController */
/* @var $model Serv */

$this->pageTitle='Панель управления - Редактировать сервер ('.$model->name.')';
?>

<h1>Редактировать сервер: <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'selectGame'=>$selectGame)); ?>

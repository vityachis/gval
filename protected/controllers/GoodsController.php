<?php

class GoodsController extends Controller
{

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','create','update','cabinet','infoSaller','cancel'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCancel($id) {
		$model = Order::model()->findByPk($id);
		$modelGoods = Goods::model()->findByPk($model->id_good);
		$modelUsers = Users::model()->findByPk($model->id_user);
		$this->render('cancel', array(
			'model'=>$model,
			'modelUsers'=>$modelUsers,
			'modelGoods'=>$modelGoods,
		));
	}

	public function actionCabinet()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$modelOrderArr = Order::model()->findAll();
		foreach ($modelOrderArr as $modelOrderOne) {
			$modelOrder[$modelOrderOne->id_good]['count'] +=1;
			$modelOrder[$modelOrderOne->id_good]['user'] = $modelOrderOne->id_user;
			$modelOrder[$modelOrderOne->id_good]['id'] = $modelOrderOne->id;
		}

		$modelUsersArr = Users::model()->findAll();
		foreach ($modelUsersArr as $modelUsersOne) {
			$modelUsers[$modelUsersOne->id]['nick'] = $modelUsersOne->nick;
			$modelUsers[$modelUsersOne->id]['id'] = $modelUsersOne->id;
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 't.user_id=:id && t.status<>:status';
		$criteria->params = array(':id'=>Yii::app()->user->id, ':status'=>2);
		$count = Goods::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '20';
		$pages->applyLimit($criteria);
		$model = Goods::model()->with('user', 'serv', 'type', 'part')->findAll($criteria);

		$this->render('cabinet', array('pages'=>$pages, 'model'=>$model, 'modelOrder'=>$modelOrder, 'modelUsers'=>$modelUsers));
	}

	public function actionInfoSaller($rules = '') {
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$infoSaller = Users::model()->findByPk(Yii::app()->user->id);
		if ($rules == 'ok') {
			if ($infoSaller->rule >= 3) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::ALREADY_SALLER_OK);
				$this->redirect(array('goods/cabinet'));
			}

			$infoSaller->rule = 3;
			if ($infoSaller->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::RULES_SALLER_OK);
				$this->redirect(array('goods/create'));
			}
		} else if ($rules == 'no') {
			if ($infoSaller->rule >= 3) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::ALREADY_SALLER_OK);
				$this->redirect(array('goods/cabinet'));
			}

			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::RULES_SALLER_NO);
			$this->redirect(array('goods/create'));
		}
		$this->render('infoSaller', array('infoSaller'=>$infoSaller));
	}

	public function actionIndex($id, $buy = null)
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}
		$modelGoods = Goods::model()->findByPk($id);
		if ($modelGoods->user_id == Yii::app()->user->id) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_BUY_THEIR_GOODS);
			$this->redirect(array('game/index', 'id'=>$modelGoods->game_id));
		}
		if ($modelGoods->status == 1) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::GOODS_ALREADY_ORDERED);
			$this->redirect(array('game/index', 'id'=>$modelGoods->game_id));
		}
		if ($modelGoods->status == 2) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::FULLY_PURCH);
			$this->redirect(array('game/index', 'id'=>$modelGoods->game_id));
		}
		if ($modelGoods->status == 0) {
			$buyGoods=$this->loadModel($id);

			if(isset($buy)) {

				// Проверяем и снимаем деньги со счета покупателя
				$buyUsers = Users::model()->findByPk(Yii::app()->user->id);

				$costGoods = $buyGoods->cost * $buyGoods->count;
				$costGoodsPercent = ($costGoods * WorkView::pricesPurch('percentPurch')) / 100;
				$sumCostGoods = $costGoodsPercent + $costGoods;
				$buyUsers->curr -= $sumCostGoods;
				if ($buyUsers->curr >= 0) {

					//Начисляем деньги в систему
					$buyUsersGoods = Users::model()->findByPk(777);
					$buyUsersGoods->curr += $sumCostGoods;

					// Меняем статус товара
					$modelGoods->status = 1;

					// Заносим товар в таблицу заказов {{order}}
					$buyOrder = new Order;
					$buyOrder->id_user = Yii::app()->user->id;
					$buyOrder->id_good = $id;
					$buyOrder->status = 1;

					if ($buyOrder->save() && $buyUsers->save() && $modelGoods->save() && $buyUsersGoods->save()) {

						// Отправляем оповещение в сообщении
						$systemMailFromSaller = new Mail;
						$systemMailFromSaller->to_id = 777;
						$systemMailFromSaller->from_id = $modelGoods->user_id;
						$systemMailFromSaller->text_mail = SystemMessage::goodsOrders($modelGoods->name);

						if (!$systemMailFromSaller->save()) {
							Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::SEND_ERROR_SYSTEM);
							$this->redirect(array('site/index'));
						}
					} else {
						Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ERROR_BUY);
						$this->redirect(array('site/index'));
					}

					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::GOODS_ORDERED);
					$this->redirect(array('site/index'));
				} else {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NOT_ENOUGH_MONEY);
					$this->redirect(array('site/index'));
				}
			}

			$model = Goods::model()->with('user','serv','type','game')->findByPk($id);

			$this->render('index',array('model'=>$model, 'buyGoods'=>$buyGoods, 'modelPrices'=>$modelPrices));
		} else {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::FULLY_PURCH);
			$this->redirect(array('site/index'));
		}
	}

	public function actionCreate()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		} else if (Users::model()->findByPk(Yii::app()->user->id)->rule < 3) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::OK_SALLER);
			$this->redirect(array('goods/infoSaller'));
		}

	    $model=new Goods;

	    $dropDownType = TypeGoods::model()->findAll();
	    $dropDownGame = Game::model()->findAll();
	    $dropDownServ = Serv::model()->findAll('game_id=1');
	    $dropDownPart = Part::model()->findAll('game_id=1');

	    // Uncomment the following line if AJAX validation is needed
	    // $this->performAjaxValidation($model);

	    if(isset($_POST['Goods']))
	    {
	        $model->attributes=$_POST['Goods'];

	        // Проверка на соответствие сервера и игры
	        $modelServ = Serv::model()->findAll('game_id=:game_id', array(':game_id'=>$model->game_id));
	        $boolServArr = 0;
	        foreach($modelServ as $modelServOne) {
	        	$model->serv_id == $modelServOne->attributes['id']?$boolServArr++:false;
	        }
	        if ($boolServArr == 0) {
	        	Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::SERVER_NO_GAME);
	        	$this->redirect(array('goods/cabinet'));
	        }

	        // Проверка на соответствие стороны и игры
	        $modelPart = Part::model()->findAll('game_id=:game_id', array(':game_id'=>$model->game_id));
	        $boolPartArr = 0;
	        foreach($modelPart as $modelPartOne) {
	        	$model->part_id == $modelPartOne->attributes['id']?$boolPartArr++:false;
	        }
	        if ($boolPartArr == 0) {
	        	Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::PART_NO_GAME);
	        	$this->redirect(array('goods/cabinet'));
	        }

	        $model->cr_date = time();
	        $model->status = 0;
	        $model->user_id = Yii::app()->user->id;
	        if ($model->save()) {
	        	Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::SUCCESS_CREATED);
	        	$this->redirect(array('goods/cabinet'));
	        }
	    }

	    $this->render('create',array(
	        'model'=>$model,
	        'dropDownType'=>$dropDownType,
	        'dropDownGame'=>$dropDownGame,
	        'dropDownServ'=>$dropDownServ,
	        'dropDownPart'=>$dropDownPart,
	    ));
	}

	public function actionUpdate($id)
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model=$this->loadModel($id);

		if ($model->user_id !== Yii::app()->user->id) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::EDIT_SOMEONE_PRODUCT);
			$this->redirect(array('goods/cabinet'));
		}
		if ($model->status == 1) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::GOODS_ORDERED_NO_EDIT);
			$this->redirect(array('goods/cabinet'));
		}
		if ($model->status == 2) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::FULLY_PURCH_NO_EDIT);
			$this->redirect(array('goods/cabinet'));
		}

		$dropDownType = TypeGoods::model()->findAll();
		$dropDownGame = Game::model()->findAll();
		$dropDownServ = Serv::model()->findAll($model->game_id);
		$dropDownPart = Part::model()->findAll($model->game_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Goods']))
		{
			$model->attributes=$_POST['Goods'];
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::CHANGES_SAVED);
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'dropDownType'=>$dropDownType,
			'dropDownGame'=>$dropDownGame,
			'dropDownServ'=>$dropDownServ,
			'dropDownPart'=>$dropDownPart,
		));
	}

	public function loadModel($id)
	{
		$model=Goods::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionAjaxDropDownListServ($id) {
		$dropDownServ = Serv::model()->findAll('game_id=:game_id', array(':game_id'=>$id));
		if (count($dropDownServ) >= 1) {
			foreach ($dropDownServ as $dropDownServOne) {
				echo '<option value="' . $dropDownServOne->id . '">' . $dropDownServOne->name . '</option>';
			}
		} else {
			echo '<option value="0">Для выбраной игры серверов нет</option>';
		}
	}

	public function actionAjaxDropDownListPart($id) {
		$dropDownPart = Part::model()->findAll('game_id=:game_id', array(':game_id'=>$id));
		if (count($dropDownPart) >= 1) {
			foreach ($dropDownPart as $dropDownPartOne) {
				echo '<option value="' . $dropDownPartOne->id . '">' . $dropDownPartOne->name . '</option>';
			}
		} else {
			echo '<option value="0">Для выбраной игры сторон нет</option>';
		}
	}
}

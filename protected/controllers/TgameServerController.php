<?php

class TgameServerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('moder'),
				'users'=>array('admin'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'top', 'create', 'topUp'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TgameServer;
		$modelGameList = Game::model()->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['TgameServer']))
		{
			$model->attributes=$_POST['TgameServer'];
			$modelUserService = Users::model()->findByPk(Yii::app()->user->id);
			$modelUserService->curr -= WorkView::pricesService('customServer');
			if ($modelUserService->curr >= 0) {
				if($model->save() && $modelUserService->save()) {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::CUSTOM_SERVER_MODER);
					$this->redirect(array('top','id'=>$model->game_id));
				} else {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ERROR_SERVICE_CURR);
					$this->redirect(array('top','id'=>$model->game_id));
				}
			} else {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_SERVER_CUSTOM_CURR);
				$this->redirect(array('top','id'=>$model->game_id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'modelGameList'=>$modelGameList,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TgameServer']))
		{
			$model->attributes=$_POST['TgameServer'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionModer() {
		$criteria = new CDbCriteria();
		$criteria->condition = 'status=:status';
		$criteria->params = array(':status'=>'0');
		$count = TgameServer::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);
		$model = TgameServer::model()->findAll($criteria);
		$this->render('moder', array('model'=>$model, 'pages'=>$pages));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id = null, $act = null)
	{
		if (Yii::app()->user->name == 'admin') {
			if ($act == 'ok') {
				$newStatus = TgameServer::model()->findByPk($id);
				$newStatus->status = 1;

				$systemMailFromModer = new Mail;
				$systemMailFromModer->to_id = 777;
				$systemMailFromModer->from_id = $newStatus->user_id;
				$systemMailFromModer->text_mail = SystemMessage::customServer($act, $newStatus->name, $newStatus->href);

				if ($newStatus->save() && $systemMailFromModer->save()) {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::MODER_CUSTOM_OK);
					$this->redirect(array('tgameServer/index'));
				}
			} else if ($act == 'no') {
				$newStatus = TgameServer::model()->findByPk($id);
				$newStatus->status = 2;

				$systemMailFromModer = new Mail;
				$systemMailFromModer->to_id = 777;
				$systemMailFromModer->from_id = $newStatus->user_id;
				$systemMailFromModer->text_mail = SystemMessage::customServer($act, $newStatus->name, $newStatus->href);

				if ($newStatus->save() && $systemMailFromModer->save()) {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::MODER_CUSTOM_NO);
					$this->redirect(array('tgameServer/index'));
				}
			}
		}

		$criteria = new CDbCriteria();
		$count = Game::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '20';
		$pages->applyLimit($criteria);
		$modelGame = Game::model()->with('user', 'serv', 'tgame')->findAll($criteria);

		$this->render('index',array(
			'modelGame'=>$modelGame,
		));
	}

	public function actionTop($id) {
		$modelGame = Game::model()->findByPk($id);

		$criteriaSite = new CDbCriteria();
		$criteriaSite->condition = 'game_id=:game_id';
		$criteriaSite->params = array(':game_id'=>$id);
		$countSite = Serv::model()->count($criteriaSite);
		$pagesSite = new CPagination($countSite);
		$pagesSite->pageSize = '25';
		$pagesSite->applyLimit($criteriaSite);
		$modelServerSite = Serv::model()->findAll($criteriaSite);

		$criteriaCustom = new CDbCriteria();
		$criteriaCustom->condition = 'game_id=:game_id && status=:status';
		$criteriaCustom->params = array(':game_id'=>$id, ':status'=>1);
		$criteriaCustom->order = 'ret DESC';
		$countCustom = TgameServer::model()->count($criteriaCustom);
		$pagesCustom = new CPagination($countCustom);
		$pagesCustom->pageSize = '25';
		$pagesCustom->applyLimit($criteriaCustom);
		$modelServerCustom = TgameServer::model()->findAll($criteriaCustom);

		$this->render('top', array(
			'modelGame' => $modelGame,
			'modelServerSite' => $modelServerSite,
			'pagesSite' => $pagesSite,
			'modelServerCustom' => $modelServerCustom,
			'pagesCustom' => $pagescustom,
		));
	}

	public function actionTopUp($id, $up, $game = null) {
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::GUEST_NO_UP_RATING);
			$this->redirect(array('site/login'));
		}
		if ($up == 'site' || $up == 'custom') {
			if ($up == 'site') {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_RATING_SERV_SITE);
				$this->redirect(array('tgameServer/top', 'id'=>$game));
			} else if ($up == 'custom') {
				$myUserModel = Users::model()->findByPk(Yii::app()->user->id);
				$myUserModel->curr -= WorkView::pricesService('serverUp');
				if ($myUserModel->curr >= 0) {
					$customServerModel = TgameServer::model()->findByPk($id);
					$customServerModel->ret++;
					$systemModel = Users::model()->findByPk('777');
					$systemModel->curr += WorkView::pricesService('serverUp');
					if ($myUserModel->save() && $customServerModel->save() && $systemModel->save()) {
						Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::RATING_UP);
						$this->redirect(array('tgameServer/index'));
					}
				} else {
					Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_RATING_UP);
					$this->redirect(array('tgameServer/index'));
				}
			}
		} else {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ERROR_ADDR_GET);
			$this->redirect(array('tgameServer/index'));
		}
		$this->render('topUp');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TgameServer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TgameServer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TgameServer $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tgame-server-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

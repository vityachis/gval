<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$count = Game::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '12';
		$pages->applyLimit($criteria);
		$gameList = Game::model()->findAll($criteria);

		$this->render('index', array('gameList'=>$gameList, 'pages'=>$pages));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$modelUser = Users::model()->findByPk(Yii::app()->user->id);
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::FEEDBACK_SUCCESS);
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model,'modelUser'=>$modelUser));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		if (!Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ALREADY_AUTH);
			$this->redirect(array('site/index'));
		}

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo TbActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionReg() {
		$model = new Users;
		$modelMyUsers = Users::model()->findByPk(Yii::app()->user->id);
		$model->scenario = 'reg';

		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if (!Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ALREADY_REG);
			$this->redirect(array('site/index'));
		}

		if(isset($_POST['Users'])) {
			$model->attributes = $_POST['Users'];
			$model->curr = 1000;
			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::SUCCESS_REG);
				$this->redirect(array('site/index'));
			}
		}
		$this->render('reg', array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		$userOffline = Users::model()->findByPk(Yii::app()->user->id);
		$userOffline->online = 0;
		$userOffline->date_last = time();
		$userOffline->save();
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionPage($view) {
		$textPageCont = Text::model()->findAll('path=:path', array('path'=>'/site/page?view=about'));
		foreach($textPageCont as $value) {
			$textPageArr[$value->id] = $value->full_text;
			$textPageArrTitl[$value->id] = $value->name;
		}
		$this->render('pages/about', array('textPageArr'=>$textPageArr, 'textPageArrTitl'=>$textPageArrTitl));
	}

	public function actionDebug() {
		echo "<center><h1>NULL</h1></center>";
	}
}

<?php

class OrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('cabinet', 'cancel', 'fullBuy'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionFullBuy($id) {

		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$modelOrderDelete = Order::model()->with('good')->findByPk($id);
		$modelGoodsDelete = Goods::model()->findByPk($modelOrderDelete->id_good);

		// Если товар уже полностью куплен - ошибка
		if ($modelGoodsDelete->status == 2) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_FULL_BUY_ORDER);
			$this->redirect(array('order/cabinet'));
		}

		$modelGoodsDelete->status = 2;
		$modelOrderDelete->status = 0;

		// Если вы не заказчик - ошибка
		if ($modelOrderDelete->id_user !== Yii::app()->user->id) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_FULL_BUY_SOMEONE);
			$this->redirect(array('order/cabinet'));
		}

		// Передаем деньги продавцу
		$buySystem = Users::model()->findByPk(777);
		$countAllDollars = $modelGoodsDelete->count * $modelGoodsDelete->cost;
		$buySystem->curr -= $countAllDollars;
		$buySaller = Users::model()->findByPk($modelOrderDelete->good->user_id);
		$buySaller->curr += $countAllDolars;

		// Отправляем сообщение об успешной сделке
		$systemMailFromSaller = new Mail;
		$systemMailFromSaller->to_id = 777;
		$systemMailFromSaller->from_id = $modelGoodsDelete->user_id;
		$systemMailFromSaller->text_mail = SystemMessage::goodsPurch($modelOrderDelete->id_user, $modelGoodsDelete->name);;
		if ($systemMailFromSaller->save() && $modelGoodsDelete->save() && $modelOrderDelete->save() && $buySaller->save() && $buySystem->save()) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::MARKED_FULL_BUY);
			$this->redirect(array('order/cabinet'));
		}
	}

	public function actionCancel($id) {

		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$modelOrder = Order::model()->findByPk($id);
		$modelGoods = Goods::model()->findByPk($modelOrder->id_good);

		// Если товар уже полностью куплен - ошибка
		if ($modelOrder->status == 2) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::CANCEL_NO_FULL_BUY);
			$this->redirect(array('order/cabinet'));
		}

		// Принимать отмену только если ее совершает заказчик
		// или продавец
		if ($modelOrder->id_user !== Yii::app()->user->id && $modelGoods->user_id !== Yii::app()->user->id) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::CANCEL_SOMEONE_ORDER);
			$this->redirect(array('order/cabinet'));
		}
		$userIdBuy = $modelOrder->id_user; // ID покупателя
		$userIdSeller = $modelGoods->user_id; // ID продавца
		$countDolars = $modelGoods->cost * $modelGoods->count;
		$modelUsersSeller = Users::model()->findByPk(777);
		$modelUsersBuy = Users::model()->findByPk($userIdBuy);
		$modelUsersSeller->curr -= $countDolars;

		// Проверяем наличие денег для возврата у продавца
		if ($modelUsersSeller->curr < 0) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::ERROR_CANCEL_SELLER);
			$this->redirect(array('order/cabinet'));
		}

		$modelUsersBuy->curr += $countDolars;
		$modelGoods->status = 0;
		$systemMailFromSaller = new Mail;
		$systemMailFromSaller->to_id = 777;
		$systemMailFromSaller->from_id = $modelGoods->user_id;
		$systemMailFromSaller->text_mail = SystemMessage::goodsCancel($modelGoods->name);

		// Сохраняем изменения в таблице {{Goods}} и удаляем строку с таблицы {{Order}}
		if ($modelGoods->save() && $modelOrder->delete() && $modelUsersBuy->save() && $modelUsersSeller->save() && $systemMailFromSaller->save()) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::CANCEL_OK);
			$this->redirect(array('order/cabinet'));
		} else {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::TIME_CANCEL_ERROR);
			$this->redirect(array('order/cabinet'));
		}
	}

	public function actionCabinet() {

		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$modelUsersArr = Users::model()->findAll();
		foreach ($modelUsersArr as $value) {
			$modelUsers[$value->id]['name'] = $value->name;
			$modelUsers[$value->id]['nick'] = $value->nick;
			$modelUsers[$value->id]['online'] = $value->online;
		}

		$modelGameArr = Game::model()->findAll();
		foreach ($modelGameArr as $value) {
			$modelGame[$value->id] = $value->name;
		}

		$modelPartArr = Part::model()->findAll();
		foreach ($modelPartArr as $value) {
			$modelPart[$value->id] = $value->name;
		}

		$modelServArr = Serv::model()->findAll();
		foreach ($modelServArr as $value) {
			$modelServ[$value->id] = $value->name;
		}

		$modelTypeGoodsArr = TypeGoods::model()->findAll();
		foreach ($modelTypeGoodsArr as $value) {
			$modelTypeGoods[$value->id] = $value->name;
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 't.id_user=:user_id && t.status=:status';
		$criteria->params = array(':user_id'=>Yii::app()->user->id, ':status'=>1);
		$count = Order::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '20';
		$pages->applyLimit($criteria);
		$model = Order::model()->with('good')->findAll($criteria);

		$this->render('cabinet', array(
			'model'=>$model,
			'pages'=>$pages,
			'modelUsers'=>$modelUsers,
			'modelGame'=>$modelGame,
			'modelServ'=>$modelServ,
			'modelTypeGoods'=>$modelTypeGoods,
			'modelPart'=>$modelPart,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Order the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Order::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Order $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

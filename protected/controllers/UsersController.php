<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','update','cabinet','fillUpPurse','fillDownPurse'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model=$this->loadModel(Yii::app()->user->id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			$model->pass = CPasswordHelper::hashPassword($model->pass);
			$model->nick = Yii::app()->user->name;
			if ($model->surname == '') $model->surname = null;
			if ($model->patron == '') $model->patron = null;
			if ($model->icq == '') $model->icq = null;
			if ($model->skype == '') $model->skype = null;
			if($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::CHANGES_SAVED);
				$this->redirect(array('cabinet'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionFillUpPurse() {
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model = Users::model()->findByPk(Yii::app()->user->id);

		$this->render('fillUpPurse', array('model'=>$model));
	}

	public function actionFillDownPurse() {
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model = Users::model()->findByPk(Yii::app()->user->id);

		$this->render('fillDownPurse', array('model'=>$model));
	}

	public function actionIndex($id)
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$revieModelCount = Revie::model()->findAll('from_id=:from_id', array(':from_id'=>$id));
		if (count($revieModelCount) >= 1) $revieModel = Revie::model()->findByPk($revieModelCount[0]->id);
		else $revieModel = new Revie;
		if (isset($_POST['Revie'])) {
			$revieModel->attributes = $_POST['Revie'];
			$revieErr = Revie::model()->findAll('from_id=:from && user_id=:user', array(':from'=>$revieModel->from_id, 'user'=>Yii::app()->user->id));

			// Если пользователь пытается оставить еще один отзыв
			if ($revieErr >= 1 && !$revieModel->isNewRecord) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::NO_TWO_REVIE);
				$this->redirect(array('index', 'id'=>$id));
			}

			// Если пользователь оставляет отзыв самому себе
			if ($revieModel->from_id == Yii::app()->user->id) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::REVIE_NO_MY);
				$this->redirect(array('index', 'id'=>$id));
			}

			// Если пользователь оставляет отзыв системе
			if ($revieModel->from_id == '777') {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::REVIE_NO_SYSTEM);
				$this->redirect(array('index', 'id'=>$id));
			}
			$revieModel->user_id = Yii::app()->user->id;
			if($revieModel->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::SUCCESS_REVIE);
				$this->redirect(array('index', 'id'=>$id));
			}
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 'from_id=:id';
		$criteria->params = array(':id'=>$id);
		$count = Revie::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);
		$revieModelAll = Revie::model()->findAll($criteria);

		if (Yii::app()->user->id == $id)
			$this->redirect('/users/cabinet');
		$model = Users::model()->findByPk($id);
		$this->render('index', array(
			'model'=>$model,
			'revieModel'=>$revieModel,
			'revieModelAll'=>$revieModelAll,
			'pages'=>$pages,
			)
		);
	}

	public function actionCabinet() {

		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model = Users::model()->findByPk(Yii::app()->user->id);
		$this->render('cabinet', array('model'=>$model));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

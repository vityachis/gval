<?php

class RevieController extends Controller
{
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		// $criteria = new CDbCriteria();
		// $criteria->condition = 'from_id=:id';
		// $criteria->params = array(':id'=>Yii::app()->user->id);
		// $count = Revie::model()->count($criteria);
		// $pages = new CPagination($count);
		// $pages->pageSize = '25';
		// $pages->applyLimit($criteria);

		// Revie::model()->updateAll(array('status'=>0), 'from_id=:my_id && status=:status', array(':my_id'=>Yii::app()->user->id, ':status'=>1));

		// $model = Revie::model()->with('user')->findAll($criteria);
		// $this->render('index', array('model'=>$model));

		$model=new Revie('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Revie']))
			$model->attributes=$_GET['Goods'];
		Revie::model()->updateAll(array('status'=>0), 'from_id=:my_id && status=:status', array(':my_id'=>Yii::app()->user->id, ':status'=>1));

		$this->render('index',array(
			'model'=>$model,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}

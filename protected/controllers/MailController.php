<?php

class MailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','hide','cabinet'),
				'users'=>array(Yii::app()->user->name),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCabinet()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 'to_id=:my_id || from_id=:my_id';
		$criteria->params = array(':my_id'=>Yii::app()->user->id);
		$criteria->order = 'id DESC';

		$modelUsers = Users::model()->findAll();
		foreach ($modelUsers as $modelUsersOne) {
			$userInfoMail[$modelUsersOne->id] = $modelUsersOne->nick;
		}

		$modelMailCount = Mail::model()->findAll('from_id=:from_id && status=:status', array(':from_id'=>Yii::app()->user->id, ':status'=>4));
		foreach ($modelMailCount as $modelMailCountOne) {
			$countNewMail[$modelMailCountOne->to_id] += 1;
		}

		$model = Mail::model()->findAll($criteria);
		$this->render('cabinet', array('model'=>$model, 'countNewMail'=>$countNewMail, 'userInfoMail'=>$userInfoMail));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionHide()
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		echo '<div style="text-align:center;font-size:60px;font-weight:bold;">Страница находиться в разработке</div>';
		echo '<div style="text-align:center;margin-top:25px;"><a href="/">На главную</a></div>';
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id)
	{
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::DANGER_NO_AUTH);
			$this->redirect(array('site/login'));
		}

		$model = new Mail;

		if(isset($_POST['Mail']))
		{
			$model->attributes=$_POST['Mail'];

			// Нельзя писать самому себе
			if ($model->from_id == Yii::app()->user->id) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::MESS_NO_SEND_MY);
				$this->redirect(array('mail/cabinet'));
			}

			// Нельзя отправить сообщение системе
			if ($model->from_id == 777) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_DANGER, AlertMess::MESS_NO_SEND_SYSTEM);
				$this->redirect(array('index', 'id'=>$model->from_id));
			}

			if($model->save())
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, AlertMess::MESS_SEND);
				$this->redirect(array('index', 'id'=>$model->from_id));
		}

		$criteria = new CDbCriteria();
		$criteria->condition = '(to_id=:my_id && from_id=:id) || (to_id=:id && from_id=:my_id)';
		$criteria->params = array(':id'=>$id, ':my_id'=>Yii::app()->user->id);
		$criteria->order = 'id DESC';
		$count = Mail::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$modelComunication = Mail::model()->findAll($criteria);
		$infoMy = Users::model()->findByPk(Yii::app()->user->id);
		$infoSomeone = Users::model()->findByPk($id);

		Mail::model()->updateAll(array('status'=>0), 'from_id=:my_id && status=:status && to_id=:to_id', array(':my_id'=>Yii::app()->user->id, ':to_id'=>$id, ':status'=>4));

		$this->render('index',array(
			'modelComunication'=>$modelComunication,
			'infoSomeone'=>$infoSomeone,
			'infoMy'=>$infoMy,
			'model'=>$model,
			'pages'=>$pages,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 28 2016 г., 11:31
-- Версия сервера: 5.7.11-log
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gameval`
--

-- --------------------------------------------------------

--
-- Структура таблицы `chv_game`
--

CREATE TABLE IF NOT EXISTS `chv_game` (
  `id` int(11) NOT NULL COMMENT 'id',
  `name` char(50) NOT NULL COMMENT 'Название игры',
  `user_id` int(11) NOT NULL COMMENT 'Создатель игры'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Таблица игр';

--
-- Дамп данных таблицы `chv_game`
--

INSERT INTO `chv_game` (`id`, `name`, `user_id`) VALUES
(1, 'World of Warcraft', 1),
(2, 'EVE Online', 1),
(3, 'Lineage 2', 1),
(4, 'Tera', 1),
(5, 'Aion', 1),
(6, 'Black Desert', 1),
(7, 'Аллоды', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_goods`
--

CREATE TABLE IF NOT EXISTS `chv_goods` (
  `id` int(11) NOT NULL COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT 'Название товара',
  `user_id` int(11) NOT NULL COMMENT 'id юзера, кто продает товар',
  `type_good` int(11) NOT NULL COMMENT 'id типа товара',
  `serv_id` int(11) DEFAULT NULL COMMENT 'id сервера, если 1 вообще или нет null',
  `part_id` int(11) DEFAULT NULL COMMENT 'id стороны, если нет null',
  `game_id` int(11) NOT NULL COMMENT 'id игры',
  `count` int(8) NOT NULL COMMENT 'Количество',
  `cost` decimal(16,2) NOT NULL COMMENT 'Стоимость за единицу',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - товар продаеться, 1 - куплен, 2 - сделка оформлена',
  `cr_date` bigint(16) NOT NULL COMMENT 'Дата создания'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Таблица товаров';

--
-- Дамп данных таблицы `chv_goods`
--

INSERT INTO `chv_goods` (`id`, `name`, `user_id`, `type_good`, `serv_id`, `part_id`, `game_id`, `count`, `cost`, `status`, `cr_date`) VALUES
(1, 'Золото', 1, 3, 1, 2, 1, 10000, '0.05', 0, 1455096372),
(2, 'Золото', 2, 3, 1, 2, 1, 1000, '0.05', 2, 1455096372),
(3, 'Золото', 3, 3, 2, 2, 1, 1000, '0.05', 0, 1455096372),
(4, 'Золото', 3, 3, 1, 2, 1, 1000, '0.05', 0, 1455096372),
(5, 'Золото', 2, 3, 1, 2, 1, 1000, '0.05', 0, 1455096372),
(6, 'Золото', 4, 3, 2, 2, 1, 1000, '0.05', 0, 1455096372),
(7, 'Золото', 2, 3, 2, 2, 1, 1000, '0.05', 0, 1455096372),
(8, 'Золото', 1, 3, 1, 2, 1, 1000, '0.05', 2, 1455096372),
(10, 'Золото', 3, 3, 2, 2, 1, 1000, '0.05', 0, 1455096372),
(11, 'Золото', 1, 3, 1, 2, 1, 1000, '0.05', 0, 1457000568),
(12, 'Золото', 1, 3, 6, 4, 5, 1000, '0.05', 0, 1457003512),
(13, 'Золото', 1, 3, 2, 1, 1, 1000, '0.05', 2, 1457003944),
(14, 'Золото', 1, 3, 2, 1, 1, 1000, '0.05', 0, 1457004090),
(15, 'Перчатка', 1, 1, 4, 0, 3, 100, '5.00', 0, 1458735687);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_mail`
--

CREATE TABLE IF NOT EXISTS `chv_mail` (
  `id` int(11) NOT NULL COMMENT 'id',
  `to_id` int(11) NOT NULL COMMENT 'Отправитель',
  `from_id` int(11) NOT NULL COMMENT 'Получатель',
  `status` int(8) NOT NULL COMMENT '0 - видят оба , 1 - не видит только отправитель, 2 - не видет только получатель, 3 - оба не видят почту, 4 - новое сообщение',
  `send_date` bigint(16) NOT NULL COMMENT 'Дата отправки',
  `text_mail` longtext NOT NULL COMMENT 'Текст сообщения'
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='Диалоги';

--
-- Дамп данных таблицы `chv_mail`
--

INSERT INTO `chv_mail` (`id`, `to_id`, `from_id`, `status`, `send_date`, `text_mail`) VALUES
(1, 3, 4, 0, 1455096372, 'Привет)'),
(2, 1, 2, 0, 1455096372, 'Здравствуй)'),
(3, 2, 1, 0, 1455096372, 'Ыы'),
(4, 1, 3, 0, 1455096372, 'Ты кто?)'),
(5, 4, 1, 0, 1455096372, 'Как твои дела?)'),
(6, 2, 1, 0, 1455096372, 'К чему бы это?'),
(7, 3, 5, 0, 1456742191, 'Как дела? Купили товар??'),
(8, 5, 2, 0, 1456742348, 'Ыы))'),
(9, 4, 5, 0, 1456742414, 'привет) Ыы))'),
(10, 1, 4, 0, 1456742496, 'Тут текст сообщения'),
(11, 3, 10, 0, 1456742608, 'Эй, ты чего?'),
(12, 4, 9, 0, 1456742633, 'Вот, написал)'),
(13, 4, 9, 0, 1456742681, 'Вот, написал)'),
(15, 6, 7, 0, 1456742921, 'ыыы'),
(16, 9, 4, 0, 1456742980, 'Текст сообщения тут.'),
(28, 1, 2, 0, 1457702958, 'привет'),
(29, 1, 2, 0, 1457703192, 'Как дела??'),
(30, 1, 2, 0, 1457703325, 'Чего не отвечаешь??'),
(31, 1, 3, 0, 1457705274, 'Аууу??'),
(32, 2, 1, 0, 1457705464, 'прост) не хочу)'),
(33, 16, 2, 0, 1457963576, 'gtrgrtgrtgrt'),
(34, 16, 2, 0, 1457963596, 'grtgggggggggggggggggggggggggggerwfrtrgggrt'),
(35, 13, 1, 0, 1458054818, 'привет\r\n'),
(36, 1, 13, 0, 1458127931, 'И тебе привет)'),
(37, 13, 1, 0, 1458127969, 'Это хорошо)'),
(38, 777, 4, 0, 1458133668, 'Ваш товар (Золото) успешно куплен и передан пользователю admin! Поздравляем с успешной сделкой!<br /><strong>Внимание!</strong> Купленный товар был удален с таблицы товаров!'),
(39, 777, 1, 0, 1458137021, 'Ваш товар (Золото) был заказан пользователем: newuser. Вы можете написать <a href="/mail/index/2">ему сообщение</a> для обсуждения условий передачи товара!'),
(40, 777, 2, 0, 1458221931, 'Ваш товар (Золото) успешно куплен и передан пользователю admin! Поздравляем с успешной сделкой!'),
(41, 777, 1, 0, 1459351375, 'Созданный вами сервер (<a href="http://myserv.com">My Serv</a>), успешно прошел модерацию и был размешен в нашем ТОП''е!'),
(42, 777, 1, 0, 1459506235, 'Ваш товар (Золото) успешно куплен и передан пользователю newuser! Поздравляем с успешной сделкой!'),
(43, 777, 1, 0, 1459847245, 'Созданный вами сервер (<a href="http://serv.er">Serv.ER</a>), успешно прошел модерацию и был размешен в нашем ТОП''е!'),
(44, 777, 1, 0, 1459847452, 'Созданный вами сервер (<a href="http://newow.gm">NewoW</a>), <strong>НЕ</strong> прошел модерацию и <strong>НЕ</strong> будет размешен в нашем ТОП''е!'),
(45, 777, 1, 0, 1459849049, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(46, 777, 1, 0, 1459849051, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(47, 777, 1, 0, 1459849054, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(48, 777, 1, 0, 1459849057, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(49, 777, 1, 0, 1459849059, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(50, 777, 1, 0, 1459849061, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(51, 777, 1, 0, 1459849063, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(52, 777, 1, 0, 1459849081, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(53, 777, 1, 0, 1459849089, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(54, 777, 1, 0, 1459849092, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(55, 777, 1, 0, 1459849095, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(56, 777, 1, 0, 1459849101, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(57, 777, 1, 0, 1459849107, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(58, 777, 1, 0, 1459849114, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(59, 777, 1, 0, 1459849118, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(60, 777, 1, 0, 1459849120, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(61, 777, 1, 0, 1459849122, 'Заказ (Золото) был отменем пользователем: loginss. И поставлен опять в список активных товаров!'),
(62, 777, 1, 0, 1459849857, 'Ваш товар (Золото) был заказан пользователем: nickkk. Вы можете написать <a href="/mail/index/7">ему сообщение</a> для обсуждения условий передачи товара!'),
(63, 777, 1, 0, 1459849875, 'Ваш товар (Золото) был заказан пользователем: nickkk. Вы можете написать <a href="/mail/index/7">ему сообщение</a> для обсуждения условий передачи товара!'),
(64, 777, 1, 0, 1459849882, 'Ваш товар (Золото) был заказан пользователем: nickkk. Вы можете написать <a href="/mail/index/7">ему сообщение</a> для обсуждения условий передачи товара!'),
(65, 777, 1, 0, 1459849978, 'Ваш товар (Золото) успешно куплен и передан пользователю nickkk! Поздравляем с успешной сделкой!'),
(66, 777, 3, 4, 1459855705, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(67, 777, 3, 4, 1459856129, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(68, 777, 3, 4, 1459856151, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(69, 777, 3, 4, 1459856498, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(70, 777, 3, 4, 1459856548, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(71, 777, 4, 4, 1459858718, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(72, 777, 2, 4, 1459858801, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(73, 777, 3, 4, 1459858803, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(74, 777, 4, 4, 1459858805, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(75, 777, 1, 0, 1459861160, 'Ваш товар (Неизвестно) был заказан пользователем: chv. Вы можете написать <a id="14" href="?r=mail/index">ему сообщение</a> для обсуждения условий передачи товара!'),
(76, 777, 1, 0, 1459861165, 'Заказ (Золото) был отменем пользователем: chv. И поставлен опять в список активных товаров!'),
(77, 777, 1, 0, 1459861410, 'Ваш товар (Неизвестно) был заказан пользователем: qwertys. Вы можете написать <a href="/mail/index/12">ему сообщение</a> для обсуждения условий передачи товара!'),
(78, 777, 1, 0, 1459861415, 'Заказ (Золото) был отменем пользователем: qwertys. И поставлен опять в список активных товаров!'),
(79, 777, 3, 4, 1460554597, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(80, 777, 3, 4, 1460699041, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(81, 777, 2, 4, 1460707804, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(82, 777, 3, 4, 1460710004, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(83, 777, 3, 4, 1460710015, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(84, 777, 2, 4, 1460710976, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(85, 777, 3, 4, 1460710986, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(86, 777, 3, 4, 1460710988, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(87, 777, 2, 4, 1460710991, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(88, 777, 3, 4, 1460711168, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(89, 777, 3, 4, 1460715471, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(90, 777, 3, 4, 1461070392, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(91, 777, 3, 4, 1461070468, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(92, 777, 3, 4, 1461323088, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(93, 777, 3, 4, 1461323104, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(94, 777, 3, 4, 1461323129, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(95, 777, 4, 4, 1461323277, 'Ваш товар (Неизвестно) был заказан пользователем: admin. Вы можете написать <a href="/mail/index/1">ему сообщение</a> для обсуждения условий передачи товара!'),
(96, 777, 3, 4, 1461568832, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(97, 777, 4, 4, 1461568834, 'Заказ (Золото) был отменем пользователем: admin. И поставлен опять в список активных товаров!'),
(98, 5, 1, 0, 1461831646, 'Привет)');

-- --------------------------------------------------------

--
-- Структура таблицы `chv_order`
--

CREATE TABLE IF NOT EXISTS `chv_order` (
  `id` int(11) NOT NULL COMMENT 'id',
  `id_good` int(11) NOT NULL COMMENT 'id товара, после смены меняеться статус',
  `id_user` int(11) NOT NULL COMMENT 'id юзера, что купил',
  `status` int(11) NOT NULL COMMENT '1 - товар в пути или еще не куплен, 0 - сделка оформлена, товар ушел'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='Таблица покупок';

--
-- Дамп данных таблицы `chv_order`
--

INSERT INTO `chv_order` (`id`, `id_good`, `id_user`, `status`) VALUES
(1, 1, 2, 0),
(9, 2, 1, 0),
(11, 13, 2, 0),
(29, 8, 7, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_part`
--

CREATE TABLE IF NOT EXISTS `chv_part` (
  `id` int(11) NOT NULL COMMENT 'id',
  `game_id` int(11) NOT NULL COMMENT 'id игры',
  `name` char(50) NOT NULL COMMENT 'Название стороны',
  `user_id` int(11) NOT NULL COMMENT 'id юзера, кто создал',
  `cr_date` bigint(16) NOT NULL COMMENT 'Дата создания'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Сторона';

--
-- Дамп данных таблицы `chv_part`
--

INSERT INTO `chv_part` (`id`, `game_id`, `name`, `user_id`, `cr_date`) VALUES
(1, 1, 'Альянс', 1, 1455096372),
(2, 1, 'Орда', 1, 1455096372),
(3, 5, 'Асмодиане', 1, 1455096372),
(4, 5, 'Элийцы', 1, 1455096372),
(5, 7, 'Империя', 1, 1455096372),
(6, 7, 'Лига', 1, 1455096372);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_prices`
--

CREATE TABLE IF NOT EXISTS `chv_prices` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `valuePrice` char(200) NOT NULL COMMENT 'Описание цены',
  `keyPrice` char(50) NOT NULL COMMENT 'Ключь для вывода',
  `price` int(11) NOT NULL COMMENT 'Цена'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Таблица с ценами';

--
-- Дамп данных таблицы `chv_prices`
--

INSERT INTO `chv_prices` (`id`, `valuePrice`, `keyPrice`, `price`) VALUES
(1, 'Создание игрового сервера', 'customServer', 50),
(2, 'Поднятия рейтинга игрового сервера', 'serverUp', 25),
(3, 'Процент за покупку товара', 'percentPurch', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_revie`
--

CREATE TABLE IF NOT EXISTS `chv_revie` (
  `id` int(11) NOT NULL COMMENT 'id',
  `from_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Для кого отзыв',
  `user_id` int(11) NOT NULL COMMENT 'Кто оставил отзыв',
  `ret` enum('1','0') NOT NULL COMMENT '+ / - от отзыва',
  `cr_date` bigint(16) NOT NULL COMMENT 'Дата создания отзыва',
  `text` varchar(255) DEFAULT NULL COMMENT 'Текст отзыва',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1 - новый, 0 - уже просмотреный'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Отзывы';

--
-- Дамп данных таблицы `chv_revie`
--

INSERT INTO `chv_revie` (`id`, `from_id`, `user_id`, `ret`, `cr_date`, `text`, `status`) VALUES
(3, 1, 13, '1', 1458565679, 'Товар успешно получен) Хороший человек))', '0'),
(4, 13, 1, '1', 1458571222, 'Хороший продавец))', '1'),
(5, 3, 1, '1', 1458734682, 'Сделка прошла успешно)', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `chv_role`
--

CREATE TABLE IF NOT EXISTS `chv_role` (
  `id` int(11) NOT NULL COMMENT 'id',
  `name` varchar(50) NOT NULL COMMENT 'Название роли'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Роли';

--
-- Дамп данных таблицы `chv_role`
--

INSERT INTO `chv_role` (`id`, `name`) VALUES
(1, 'Пользователь'),
(2, 'Покупатель'),
(3, 'Продавец'),
(4, 'Модератор'),
(5, 'Администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `chv_serv`
--

CREATE TABLE IF NOT EXISTS `chv_serv` (
  `id` int(11) NOT NULL COMMENT 'id',
  `game_id` int(11) NOT NULL COMMENT 'id игры',
  `name` char(50) NOT NULL COMMENT 'Имя сервера',
  `user_id` int(11) NOT NULL COMMENT 'id кто создал',
  `cr_date` bigint(16) NOT NULL COMMENT 'Дата создания',
  `up_date` bigint(16) NOT NULL COMMENT 'Дата изменения'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Сервер';

--
-- Дамп данных таблицы `chv_serv`
--

INSERT INTO `chv_serv` (`id`, `game_id`, `name`, `user_id`, `cr_date`, `up_date`) VALUES
(1, 1, 'Дракономор', 1, 1455096372, 1455096372),
(2, 1, 'Черный Шрам', 1, 1455096372, 1455096372),
(3, 3, 'Athebalt + Esthus', 1, 1455096372, 1455096372),
(4, 3, 'Blackbird + Ramsheart', 1, 1455096372, 1455096372),
(5, 4, 'Арборея (PVE)', 1, 1455096372, 1455096372),
(6, 5, 'Гардарика', 1, 1455096372, 1455096372);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_text`
--

CREATE TABLE IF NOT EXISTS `chv_text` (
  `id` int(11) NOT NULL,
  `path` char(50) NOT NULL,
  `name` char(50) DEFAULT NULL,
  `full_text` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `cr_date` bigint(16) NOT NULL,
  `up_date` bigint(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chv_text`
--

INSERT INTO `chv_text` (`id`, `path`, `name`, `full_text`, `user_id`, `cr_date`, `up_date`) VALUES
(1, '/', 'Добро пожаловать', '<p>\r\n	Добро пожаловать на первую в рунете биржу игровых ценностей GameVal. Пришло время покупать напрямую у других игроков без лишних посредников и переплат! <b>Все сделки защищены гарантом CashPass</b> (подробнее на странице &laquo;<a href="/site/page?view=about">Помощь</a>&raquo;). Будьте на связи!</p>\r\n', 1, 1455096372, 1461323861),
(2, '/site/page?view=about', 'Помощь', '<h4>\r\n	Какие преимущества у GameVal перед обычным магазином?</h4>\r\n<p>\r\n	<b>1. Самая лучшая цена.</b></p>\r\n<p>\r\n	Поскольку GameVal - это биржа игровых ценностей, каждый продавец стремится продать игровую ценность как можно дешевле. Кроме того, комиссия GameVal составляет всего около 5%, что существенно меньше, чем у любого магазина игровых валют.</p>\r\n<p>\r\n	<b>2. Мгновенная доставка.</b></p>\r\n<p>\r\n	Поскольку вы покупаете игровые ценности непосредственно у того, кто добыл её в игре, вы получаете не только лучшую цену, но и мгновенную доставку - ведь игровая ценности в наличии, и, поэтому, доставлена будет в самый короткий срок. Ни один магазин игровых валют не может обеспечить подобный сервис.</p>\r\n<p>\r\n	<b>3. Безопасность.</b></p>\r\n<p>\r\n	Обычно издатели находят IP/HWID тех, кто перепродает, отслеживают все их аккаунты, после чего блокируют как покупателей, так и продавцов. Принцип работы GameVal исключает подобные блокировки.</p>\r\n<h4>\r\n	<br />\r\n	Как происходит сделка в GameVal?</h4>\r\n<p>\r\n	На бирже игровых валют GameVal используется тот же принцип, что и в нашем <a href="http://www.cashpass.ru">гаранте сделок CashPass</a>. <b>CashPass провел уже более 4000 успешных сделок, и эта схема хорошо зарекомендовала себя в данной тематике:</b></p>\r\n<ol>\r\n	<li>\r\n		<p>\r\n			Покупатель должен зарегистрироваться на нашем сайте.</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			Покупатель должен выбрать интересующее его предложение в таблице, связаться с продавцом, произвести оплату.</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			Продавец передает игровую валюту.</p>\r\n	</li>\r\n	<li>\r\n		Покупатель окончательно завершает перевод средств продавцу.</li>\r\n</ol>\r\n<h4>\r\n	<br />\r\n	Защищает ли GameVal от блокировок за покупку игровой валюты?</h4>\r\n<p>\r\n	Как и любой гарант-сервис, GameVal гарантирует лишь процесс сделки. Вопросы блокировок покупателям следует обсуждать с каждым продавцом отдельно - все продавцы разные, у всех разные условия, различный стаж и различные гарантии.</p>\r\n<p>\r\n	После совершения оплаты, покупатель имеет возможность написать отзыв о работе продавца, в любое время. В том случае, если у продавца нет негативных отзывов и старая дата регистрации, он более надежен, чем другие.</p>\r\n<p>\r\n	Покупатель так же имеет возможность написать положительный отзыв.</p>\r\n<h4>\r\n	<br />\r\n	Как разрешаются споры?</h4>\r\n<p>\r\n	<b>Что будет, если покупатель получит валюту, но не завершит заказ? Что будет, если продавец &quot;пропадет&quot; после пополнения баланса заказа? Что будет, если продавец не передаст игровую валюту и не нажмет кнопку возврата средств? </b></p>\r\n<p>\r\n	Все виды споров и инцидентов разрешает сервис арбитража GameVal (CashPass).</p>\r\n<p>\r\n	Арбитраж несет материальную ответственность за разрешение споров в системе GameVal (CashPass) в размере, двукратно превышающим сумму сделки. Таким образом, в случае какой-либо ошибки в разрешении спора, сервис обязуется компенсировать все убытки (как прямые, так и косвенные).</p>', 1, 1455615296, 1457102290);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_tgame_server`
--

CREATE TABLE IF NOT EXISTS `chv_tgame_server` (
  `id` int(11) NOT NULL COMMENT 'Примари кей',
  `name` varchar(250) NOT NULL COMMENT 'Имя игрового сервера',
  `ret` int(11) NOT NULL COMMENT 'Рейтинг',
  `game_id` int(11) NOT NULL COMMENT 'ИД игры',
  `status` enum('1','0','2') NOT NULL DEFAULT '0' COMMENT 'Статус модерации: 1 прошел, 0 ожидает, 2 отменен',
  `href` char(255) NOT NULL COMMENT 'Ссылка на пользовательский игровой сервер',
  `user_id` int(11) NOT NULL COMMENT 'ИД того кто создал',
  `cr_date` int(10) NOT NULL COMMENT 'Дата создания',
  `moder_id` int(11) DEFAULT NULL COMMENT 'ИД того кто опубликовал'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Таблица игровых серверов';

--
-- Дамп данных таблицы `chv_tgame_server`
--

INSERT INTO `chv_tgame_server` (`id`, `name`, `ret`, `game_id`, `status`, `href`, `user_id`, `cr_date`, `moder_id`) VALUES
(1, 'Мой сервер', 4, 1, '1', 'http://my.serv.ru', 1, 1459172025, 1),
(2, 'My Serv', 5, 1, '1', 'http://myserv.com', 1, 1459348006, 1),
(3, 'Serv.ER', 1, 7, '1', 'http://serv.er', 1, 1459847225, NULL),
(4, 'NewoW', 0, 1, '2', 'http://newow.gm', 1, 1459847390, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_type_goods`
--

CREATE TABLE IF NOT EXISTS `chv_type_goods` (
  `id` int(11) NOT NULL COMMENT 'id',
  `name` char(50) NOT NULL COMMENT 'Название типа товара',
  `user_id` int(11) NOT NULL COMMENT 'id кто создал',
  `cr_date` bigint(16) NOT NULL COMMENT 'Дата создания',
  `up_date` bigint(16) NOT NULL COMMENT 'Дата изменения'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Типы товаров';

--
-- Дамп данных таблицы `chv_type_goods`
--

INSERT INTO `chv_type_goods` (`id`, `name`, `user_id`, `cr_date`, `up_date`) VALUES
(1, 'Предмет', 1, 1455096372, 1455096372),
(2, 'Аккаунт', 1, 1455096372, 1455096372),
(3, 'Валюта', 1, 1455096372, 1455096372);

-- --------------------------------------------------------

--
-- Структура таблицы `chv_users`
--

CREATE TABLE IF NOT EXISTS `chv_users` (
  `id` int(11) NOT NULL COMMENT 'id',
  `vk_id` char(50) DEFAULT NULL COMMENT 'id страницы vk',
  `nick` char(16) NOT NULL COMMENT 'Ник пользователя',
  `pass` char(200) NOT NULL COMMENT 'Кэш пароля',
  `pol` enum('1','2') NOT NULL DEFAULT '1' COMMENT 'Пол:  1 - мужской, 2 - женский',
  `rule` enum('1','2','3','4','5') NOT NULL DEFAULT '2' COMMENT 'Зарегистрированый (1), покупатель (2), продавец (3), модератор (4), администратор (5)',
  `ban` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1 - бан, 0 - нет бана',
  `ban_time` bigint(16) DEFAULT '0' COMMENT 'Время когда дали бан',
  `lvl_ban` enum('0','3600','10800','21600','86400','604800') NOT NULL DEFAULT '0' COMMENT 'Время бана: 1 час, 3 часа, 6 часов, 1 день, 7 дней',
  `email` char(50) NOT NULL COMMENT 'Email пользователя',
  `icq` int(16) DEFAULT NULL COMMENT 'ICQ пользователя',
  `skype` char(50) DEFAULT NULL COMMENT 'Skype пользователя',
  `date_reg` bigint(16) NOT NULL COMMENT 'Дата регистрации',
  `date_last` bigint(16) unsigned NOT NULL COMMENT 'Последний вход',
  `name` char(16) NOT NULL COMMENT 'Имя',
  `surname` char(32) DEFAULT NULL COMMENT 'Фамилия',
  `patron` char(32) DEFAULT NULL COMMENT 'Очество',
  `online` enum('1','0') NOT NULL DEFAULT '0' COMMENT 'Онлайн: 1 - он, 0 - офф',
  `curr` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT 'Деньги пользователя'
) ENGINE=InnoDB AUTO_INCREMENT=778 DEFAULT CHARSET=utf8 COMMENT='Юзер';

--
-- Дамп данных таблицы `chv_users`
--

INSERT INTO `chv_users` (`id`, `vk_id`, `nick`, `pass`, `pol`, `rule`, `ban`, `ban_time`, `lvl_ban`, `email`, `icq`, `skype`, `date_reg`, `date_last`, `name`, `surname`, `patron`, `online`, `curr`) VALUES
(1, NULL, 'admin', '$2y$13$jLJ9jDVO0nB7q8n0izmDge6ZC/eq7u/uTWK.UVRb.T7oQQH3SangK', '1', '5', '0', 0, '0', 'vityachis@ya.ru', NULL, NULL, 1455096372, 1461831658, 'Виктор', NULL, NULL, '1', '1212321321401.00'),
(2, NULL, 'newuser', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '3', '0', 0, '0', 'newuser@ya.ru', NULL, NULL, 1455096372, 1458137027, 'Андрейка', NULL, NULL, '0', '10000.00'),
(3, NULL, 'qwerty', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'qwerty@qw.er', NULL, NULL, 1455096372, 1455096372, 'qwerty', NULL, NULL, '0', '9950.00'),
(4, NULL, 'newwee', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'newwee@ya.ru', NULL, NULL, 1455096372, 1458133957, 'Машка', NULL, NULL, '0', '10050.00'),
(5, NULL, 'loginss', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'petya@mail.ru', NULL, NULL, 1455802830, 1461831651, 'Петя', NULL, NULL, '0', '10850.00'),
(7, NULL, 'nickkk', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'nick@ua.ua', NULL, NULL, 1455869808, 1459849987, 'Никккк', NULL, NULL, '0', '10000.00'),
(8, NULL, 'bitye', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'bit@in.ua', NULL, NULL, 1455870289, 1455870289, 'Битуе', NULL, NULL, '0', '10000.00'),
(9, NULL, 'Niickk', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'niikk@kil.ri', NULL, NULL, 1455876268, 1455876268, 'никка', NULL, NULL, '0', '10000.00'),
(10, NULL, 'nickknew', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'serg@yandex.com', NULL, NULL, 1456556297, 1456556297, 'Сергей', NULL, NULL, '0', '10000.00'),
(12, NULL, 'qwertys', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '1', '2', '0', 0, '0', 'qwerty@qwer.tys', NULL, NULL, 1456556609, 1459861437, 'Йцукегн', NULL, NULL, '0', '10000.00'),
(13, NULL, 'Anna', '$2y$13$gjAtaH4bxMdFIVtAtA7Hwu6fQkIAE.4Sgv7X.6TLcfC/5RFRH8XcG', '2', '2', '0', 0, '0', 'anna@ya.ru', NULL, NULL, 1456559975, 1458566042, 'Анна', NULL, NULL, '0', '10000.00'),
(14, NULL, 'chv', '$2y$13$YI0ay6khR.tu7av9isUqJ.9uMwXrmKVxUK01BMSxziyQC/vIfwNWe', '1', '2', '0', 0, '0', 'chv@yandex.ru', NULL, NULL, 1457078856, 1459861167, 'Виктор', NULL, NULL, '0', '10000.00'),
(15, NULL, 'Hids', '$2y$13$xdUicUrqlD.Qyvme4lquR.bpxobbHtuzZ1LkLSVidZA66lOQSS4tG', '1', '3', '0', 0, '0', 'hids@ya.ru', NULL, NULL, 1457014684, 1459773946, 'Хидс', NULL, NULL, '0', '10000.00'),
(16, NULL, 'Vova', '$2y$13$hdjQeC0c.RiJmecSARLgvOk9/plpr2kbqQPD.bIiOeW34XLgt7yAO', '1', '2', '0', 0, '0', 'xodiachih@yandex.ru', NULL, NULL, 1457962343, 1457963606, 'Владимир', NULL, NULL, '0', '9950.00'),
(777, NULL, 'System', 'System', '1', '1', '0', 0, '0', 'system@gval.info', NULL, NULL, 0, 0, 'Система', NULL, NULL, '1', '2565645485.00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `chv_game`
--
ALTER TABLE `chv_game`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_goods`
--
ALTER TABLE `chv_goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_mail`
--
ALTER TABLE `chv_mail`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_order`
--
ALTER TABLE `chv_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_good` (`id_good`);

--
-- Индексы таблицы `chv_part`
--
ALTER TABLE `chv_part`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_prices`
--
ALTER TABLE `chv_prices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`keyPrice`);

--
-- Индексы таблицы `chv_revie`
--
ALTER TABLE `chv_revie`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_role`
--
ALTER TABLE `chv_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_serv`
--
ALTER TABLE `chv_serv`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_text`
--
ALTER TABLE `chv_text`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_tgame_server`
--
ALTER TABLE `chv_tgame_server`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_type_goods`
--
ALTER TABLE `chv_type_goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chv_users`
--
ALTER TABLE `chv_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nick` (`nick`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `chv_game`
--
ALTER TABLE `chv_game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `chv_goods`
--
ALTER TABLE `chv_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `chv_mail`
--
ALTER TABLE `chv_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT для таблицы `chv_order`
--
ALTER TABLE `chv_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `chv_part`
--
ALTER TABLE `chv_part`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `chv_prices`
--
ALTER TABLE `chv_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `chv_revie`
--
ALTER TABLE `chv_revie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `chv_role`
--
ALTER TABLE `chv_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `chv_serv`
--
ALTER TABLE `chv_serv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `chv_text`
--
ALTER TABLE `chv_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `chv_tgame_server`
--
ALTER TABLE `chv_tgame_server`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Примари кей',AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `chv_type_goods`
--
ALTER TABLE `chv_type_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `chv_users`
--
ALTER TABLE `chv_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=778;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
